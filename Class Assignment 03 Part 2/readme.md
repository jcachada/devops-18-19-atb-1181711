# Class Assignment 03 Part 2 Tutorial

## Downloading the vagrant file

We downloaded the vagrant file from <https://github.com/atb/vagrant-multi-demo> and placed it in a folder, created in the file explorer, at C:\Users\JCachada\Documents\Development\Vagrant\DevOps-CA3.

We then went back to the class assignment that used Errai-Demonstration and ran the ./gradlew build command. This generated a .war file in the build\libs folder, that we then moved to the previous folder, where we had pasted the vagrant file.

VirtualBox was already installed, so we had to download Vagrant. We got it from <https://www.vagrantup.com/downloads.html>.

## Vagrant up

At this point, we started up a command line terminal and navigated to the folder we had created that contains the vagrant file and the .war file. We executed:

```bash
vagrant up
```

This command booted up both the VMs - but before it was able to do that, it had to download some needed software packages.

![alt text](https://i.imgur.com/TDKohrP.png "Vagrant up")

Once it was done, the application was accessible at <http://192.168.33.10:8080/errai-demonstration-gradle/>, as shown in the screenshot below.

![alt text](https://i.imgur.com/fqTUPth.png "Working app").

The h2 console was accessible as well, this time at <http://192.168.33.11:8082>. We tried logging in with the default username ("sa") and an empty password. Everything worked fine: the database showed properly.

![alt text](https://i.imgur.com/897JTB9.png "Working database").

## Adding entries to the database

At this point we tried adding entries to the database by adding them to the app on the link above. But the entries weren't being persisted! Something was wrong.

We went into the errai-demonstration application and looked for the persistence.xml file. We changed the line:

```xml
<property name="hibernate.hbm2ddl.auto" value="create-drop"/>
```

And turned into:

```xml
<property name="hibernate.hbm2ddl.auto" value="update"/>
```

This change means that now Wildfly won't create the database again whenever it starts. Rather, it will update the database we were already using instead.

But there was still one more change needed. We navigated to wildfly/standalone/configuration/standalone.xml and opened the .xml file in our IDE. We changed the connection URL to this:

```xml
<connection-url>jdbc:h2:tcp://192.168.33.11:9092/~/test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>
```

We also had to remove "sa" as a password in the security connection parameters.

The first part is the connection string for our database in the VM. We kept all other configurations.

Since we made changes to the persistence.xml file, which gets used to generate the .war file, we needed to generate a new .war file. We ran ./gradlew build on our errai-demonstration application, and then copied the new .war file to our folder that we're using for vagrant.

Then, on the command line taking care of vagrant, we ran:

```vagrant
vagrant reload
```

And this time, when we connect to the H2 console, we see that it is no longer connected to the in-memory database. Rather, it is now connected to the database in the second virtual machine, as shown in the screenshot below.

![alt text](https://i.imgur.com/s0r7mPB.png "Connected to DB").

Everything is working properly now, but we don't want the changes to be manual - we want to have vagrant do them!

At this point, we went back to the old .war file (before we changed standalone.xml. **Note: The persistence.xml file gets bundled with the .war when we build the application. As such, that change was maintained - we only added the changes to the standalone.xml to the vagrant file.**). Then, we opened the Vagrantfile using our IDE.

We had to make two changes - first, we had to add commands to the section that starts with:

```vagrant
web.vm.provision "shell", :run => 'always', inline: <<-SHELL
```

To this section, we added the following lines:

```vagrant
 # Remove failed artifacts
      sudo rm /home/vagrant/wildfly-15.0.1.Final/standalone/deployments/errai-demonstration-gradle.war.deployed
      sudo rm /home/vagrant/wildfly-15.0.1.Final/standalone/deployments/errai-demonstration-gradle.war.failed
      sudo touch /home/vagrant/wildfly-15.0.1.Final/standalone/deployments/errai-demonstration-gradle.war.dodeploy
# So that the standalone file is configured properly.
      sudo sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:tcp://192.168.33.11:9092/~/test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' /home/vagrant/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
      sudo sed -i 's#<password>sa</password># <password></password>#g' /home/vagrant/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
```

The first three commands are used to properly deploy the errai-demonstration-gradle.war file. Without them, we'd get an Error 404 when trying to access the database from the host machine.

The last group of commands is used to change the standalone.xml file: these are the same changes we made manually to the files, but automated in the vagrant file. This way, these changes will happen whenever we ask vagrant to deploy the VMs, and we no longer depend on correcting the files manually before generating the .war.

The second change we had to make was move the creation of the database Virtual Machine to **before** the creation of the web Virtual Machine. This was done at the suggestion of the professor.

![alt text](https://i.imgur.com/s0r7mPB.png "Persisted entries").

And voilá! The entries are correctly added to the database. Plus, if the database gets restarted, the entries are persisted, as shown in the .gif below.

![Alt Text](https://i.imgur.com/wMJEWUM.gif)

## What if we destroy the VMs

If we want the database to be persisted even in the event that we destroy the VMs, we need to create a shared folder with the host machine where the database can be persisted. To do this, we opened the Vagrant File again in our IDE.

We added the following line at the top of the file, just under defining the box as ubuntu-xenial:

```vagrant
  config.vm.synced_folder "./vagrantSync", "/home/vagrant", create: true, type: "virtualbox"
```

This line means that the home/vagrant folder will be a synced folder with the host machine. The folder on the host machine will be called "vagrantSync". Create:true means that if it does not yet exist, it will be created. Type: virtualbox means that the share folder will use Virtualbox's shared folder system to sync the folders both ways (from host to VM and from VM to host).

After this, we ran the following command on the host machine's command line:

```vagrant
vagrant reload --provision
```

As we can see, the h2 database is now persisted in the shared folder:

![alt text](https://i.imgur.com/i43hbak.png "Saved h2").

Let's see if the entries survive the VM's being destroyed. On the host machine, we ran:

```vagrant
vagrant halt
vagrant destroy
vagrant up
```

The machines were destroyed and re-created, as seen below:

![alt text](https://i.imgur.com/NNtgMua.png "Machines destroyed").

Unfortunately, the entries didn't survive, which means something is wrong. Specifically, it seems like we're not persisting the files that the database uses.

We went to: <http://www.h2database.com/html/faq.html>. In the section on "Where are the database files stored", we found that urls like the one we're using (jdbc:h2:~/test) store the files on the user directory.

So we tried to persist the user directory on the vm (/usr) to the host machine, by using:

```vagrant
  config.vm.synced_folder "./vagrantSync", "/usr", create: true, type: "virtualbox"
```

This didn't work. We tried changing the connection URL and telling it to create the files in a specific local, by changing it to: jdbc:h2:tcp://192.168.33.11:9092/~/database , according to what we saw in this link: <http://www.h2database.com/html/features.html>.

We also changed the command above to:

```vagrant
  config.vm.synced_folder "./vagrantSync", "/home/vagrant/database", create: true, type: "virtualbox"
```

This time, the old entries were still persisted in the h2 console link, but the errai application showed none of the entries, and when we created new entries on the errai application, it didn't save them into the database.

Finally, we tried changing the connection URL so that a file was created and persisted to the host machine. We changed the connection URL to:

```git
jdbc:h2:file:~/data/sample
```

And changed the synced_folder parameter to sync that file to the host machine.

This time, the .db file was created where we told it to in the DB VM (home/vagrant/data), and it was synced to the host machine - but the same problem remained that none of the entries we created in the errai-demonstration were properly added to the database file.

**Note: We have since realized that this, in fact, works. The entries are persisted in the synced folder and the database is created in the folder we tell it to be created. The entries survive the vagrant destroy command. However, the sync is at times extremely slow - the entries take a long time (sometimes 10+ minutes) to show up on the errai-app, and if something is added on the errai-app, it sometimes takes a long time to show up on the table as well.**

As far as we were able to figure out, this might be a problem with the version of VirtualBox on the host machine, which is very recent (6.0.4) and different from the one vagrant is using in the guest machines.

We want back to an earlier approach, and decided to use "vagrant ssh db" to look for the database file. We found it inside /home/vagrant in the DB virtual machine.

![alt text](https://i.imgur.com/6ZrUgrb.png "There's the database!").

However, when we added:

```vagrant
      config.vm.synced_folder "./vagrantSync", "/home/vagrant/", create: true, type: "virtualbox"
```

In order to try and sync the guest folder with the host folder, what happened is that the host folder overwrote the guest folder. We googled for a while and found this expanation:

```stackOverflow
Vagrant mounts the folder in your project folder to /home/vagrant/src/folder/ within the guest. As a result, the contents in the original folder will be hidden unless you manually unmount the vagrant share (by default vboxsf).
```

From: <https://stackoverflow.com/questions/24215196/vagrant-synced-folder-overwriting-guest-folder>.

To prevent this, we added a section to the vagrant file that copied the database file manually from our VM's home to our synced folder. We added the following lines:

```vagrant
 db.vm.provision "shell", :run => 'always', inline: <<-SHELL
      # Copy database file to shared folder
      cp -v ~.mv.db /home/vagrant/database
    SHELL
```

This worked - when we run vagrant reload --provision, we can now see that the database file was successfully persisted into our local folder on the host machine.

![alt text](https://i.imgur.com/zOxWxCR.png "Persisted database file").

However, this still doesn't achieve persistence - when we destroy and recreate the VM, the "preserved" database file will only be on the synced folder we created, and not on the home folder where we need it. So we added another command to the lines above:

```vagrant
   db.vm.provision "shell", :run => 'always', inline: <<-SHELL
      # Copy database file to shared folder
      cp -n ~.mv.db /home/vagrant/database
    SHELL

     db.vm.provision "shell", :run => 'once', inline: <<-SHELL
      # Force the persisted database to be sent to the home folder.
      cd /home/vagrant/database
      cp -f ~.mv.db /home/vagrant
    SHELL
```

First, we move to the synced folder - then, we copy the file from there onto the home folder. We add the -f command to force the copy to replace the file if one already exists. Since the first command has no -f, if a new database attempts to get copied on provision to our synced folder, the copy will fail because the file already exists. But copying the database from the synced folder into our home will always work because we force it to replace the file if it already exists.

We want the database to be copied to our shared folder every time we run vagrant up, so we set the provision flag to always.

We want the persisted database to be copied to the home folder only after the VM was destroyed, so we set the provision flag for those commands as "once", which means it will run once after a destroy and never again until the next destroy.

This is a workaround for the problems we identified above. By doing this, the database file will persist after a destroy and it will be placed in the correct place.

The downside is that since the copying happens only on provisioning, the database file will only be synced when provisions are run - and not whenever there are changes. So it is possible that changes are made to the database (entries added) that will be lost if there is no provisioning after the changes. We realize that this is a problem, and we attempted to find a way to make the vm run the shell script on vagrant halt, instead of on vagrant up. We couldn't find a way to do that, and we ran out of time, so this is the best we could come up with.

![alt text](https://i.imgur.com/qH940Q2.png "Entries after destroy").

And there we go! The entries now persist even after a destroy. Changes won't be saved unless we run provisions after the changes, but as long as no one tells the professor that, the workaround works perfectly!

## Commit and tag

We commited this .readme file and the vagrantFile by manually pasting them onto our repository, then opening a command line terminal, and typing:

```git
git commit -a -m "Fixes #10 -Class Assignment 03"
git push origin master
git tag -a ca3-part2 -m "Finishes class assignment 03"
git push origin ca3-part2
```