# Class Assignment 05 Part 2

## Jenkinsfile

Part 2 of the class assignment is very similar to part 1, except with extra steps to be performed in the Jenkinsfile. This time we also need to use Part 2 of Class Assignment 02 (in part 1 of ca5, we used the first part of ca2, but now we need the renderPlantUml task which is only in part 2), so we need to change the working directory in our Jenkinsfile.

We also had to, like in part 1, manually add the gradle wrapper .war to the repository, since it wasn't committed due to our general gitignore.

## Creating the Pipeline

After starting jenkins, we started a new Job, chose "Pipeline", and selected "From SCM script". We added the bitbucket credentials to our repository and the path to our jenkinsfile. The final configuration screen looked like this:

![alt text](https://i.imgur.com/dbuXmsS.png "Pipeline configuration.")

## Running the pipeline

We had a problem with our docker container that had the pipeline running out of memory:

```gradle
* What went wrong:
Execution failed for task 'gradleCompile'.
> Process 'gradleCompile' finished with non-zero exit value 137
```

To fix this, we had to rerun the Docker container with the -m 4g annotation to allocate 4GB of memory to it, which is probably overkill, but we wanted to be safe.

```docker
 docker run -p 8080:8080 -p 50000:50000 -m 4g jenkinsci/blueocean
```

With this, things finally worked properly.

Our Jenkinsfile looked like this:

```groovy
pipeline

    {
        agent any

        stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId:'Bitbucket', url:
                'https://jcachada@bitbucket.org/jcachada/devops-18-19-atb-1181711.git'
            }
        }
        stage('Assemble') {
            steps {
                dir('ca2-part2/Errai-Demonstration') {
                    echo 'Building...'
                    sh 'chmod +x gradlew'
                    sh './gradlew build -x test '
                }
            }
        }
        stage('Test') {
            steps {
                dir('ca2-part2/Errai-Demonstration') {
                echo 'Building...'
                sh 'chmod +x gradlew'
                sh './gradlew test'
                sh 'touch build/test-results/test/*.xml'
                junit 'build/test-results/test/*.xml'
            }
            }
        }
        stage('Javadoc') {
            steps {
                dir('ca2-part2/Errai-Demonstration') {
                echo 'Generating javadocs...'
                sh 'chmod +x gradlew'
                sh './gradlew copyUML'
                publishHTML (target: [
                allowMissing: false,
                keepAll: true,
                reportDir: 'build/docs/javadoc',
                reportFiles: 'overview-summary.html',
                reportName: "Javadocs"
                ])
            }
            }
        }
        stage('Archiving') {
            steps {
                dir('ca2-part2/Errai-Demonstration') {
                sh 'pwd'
                echo 'Archiving...'
                archiveArtifacts 'build/libs/'
            }
            }
        }
    }}
```

The javadocs were published successfully, as shown in the screenshot below:

![alt text](https://i.imgur.com/y8oQgzf.png "Javadocs.")

## Publishing the image

To publish the image, we created a Dockerfile with the following code:

```bash
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

COPY /ca2-part2/Errai-Demonstration/build/libs/Errai-Demonstration.war /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/

RUN wget http://download.jboss.org/wildfly/15.0.1.Final/wildfly-15.0.1.Final.tar.gz && \
  tar -xvzf wildfly-15.0.1.Final.tar.gz && \
  sed -i 's#<inet-address value="${jboss\.bind\.address:127\.0\.0\.1}"/>#<any-address/>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
  sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
  sed -i 's#<password>sa</password>#<password></password>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml

CMD /usr/src/app/wildfly-15.0.1.Final/bin/standalone.sh
```

Then we added the following two stages to our Jenkinsfile:

```groovy
stage('Docker Image') {
            steps {
              dir('ca5-part2') {
                    script {
                    dockerimage = docker.build("jcachada/devopstestca5:${env.BUILD_ID}")
                }
                }
            }
        }
    }
        stage('Publish Image') {
            steps {
                dir('ca5-part2') {
                    script{
                        sh 'docker login -u <username> -p <password>'
                        dockerimage.push
                    }
                }
            }
    }
```

When we ran the pipeline now, everything worked properly, and the image was pushed onto our docker hub, as shown below. It was tagged with the number of the build, which was build #15.

![alt text](https://i.imgur.com/Tmc6Dhu.png "Image published.")

## Tagging the repository

At this point we committed the readme and tagged the commit with ca5-part2, as we had been doing so far.

```git
git tag -a ca5-part2 -m "Finishes second part of class assignment."
git push origin ca5-part2
```