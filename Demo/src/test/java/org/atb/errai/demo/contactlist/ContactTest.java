package org.atb.errai.demo.contactlist;

import org.atb.errai.demo.contactlist.client.shared.Contact;
import org.junit.Test;

import static org.junit.gen5.api.Assertions.assertEquals;

public class ContactTest {
    // Common testing artifacts for testing in this class.

    private Contact validContact = new Contact(1, "Contact", "contact@contact.com", "221234567");

    @Test
    public void seeIfGetSetIDWork() {
        // Arrange

        long expectedResult = 31;
        validContact.setId(31);

        // Act

        long actualResult = validContact.getId();

        // Assert

        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void seeIfGetSetNameWork(){
        // Arrange

        String expectedResult = "Mock";
        validContact.setName("Mock");

        // Act

        String actualResult = validContact.getName();

        // Assert

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void seeIfGetSetPhoneNumberWork(){
        // Arrange

        String expectedResult = "913332112";
        validContact.setPhoneNumber("913332112");

        // Act

        String actualResult = validContact.getPhoneNumber();

        // Assert

        assertEquals(expectedResult, actualResult);
    }

    @Test
    public void seeIfConstructorWorksWithID(){
        // Arrange

       validContact = new Contact(3, "Alexandre", "atb@isep.ipp.pt", "221234567");

        // Act

        long actualID = validContact.getId();
        String actualName = validContact.getName();
        String actualEmail = validContact.getEmail();
        String actualPhoneNumber = validContact.getPhoneNumber();

        // Assert

        assertEquals(3L, actualID);
        assertEquals("Alexandre", actualName);
        assertEquals("atb@isep.ipp.pt", actualEmail);
        assertEquals("221234567", actualPhoneNumber);
    }

    @Test
    public void seeIfConstructorWorksWithoutID(){
        // Arrange

        validContact = new Contact("Alexandre", "atb@isep.ipp.pt", "221234567");

        // Act

        String actualName = validContact.getName();
        String actualEmail = validContact.getEmail();
        String actualPhoneNumber = validContact.getPhoneNumber();

        // Assert

        assertEquals("Alexandre", actualName);
        assertEquals("atb@isep.ipp.pt", actualEmail);
        assertEquals("221234567", actualPhoneNumber);
    }

    @Test
    public void seeIfConstructorWorksInvalidEmail(){
        // Arrange

        validContact = new Contact("Alexandre", "atbisep.ipp.pt", "221234567");

        // Act

        String actualEmail = validContact.getEmail();

        // Assert

        assertEquals("Invalid e-mail.", actualEmail);
    }

    @Test
    public void seeIfConstructorWithIDWorksInvalidEmail(){
        // Arrange

        validContact = new Contact(2L, "Alexandre", "atbisep.ipp.pt", "221234567");

        // Act

        String actualEmail = validContact.getEmail();

        // Assert

        assertEquals("Invalid e-mail.", actualEmail);
    }

    @Test
    public void seeIfConstructorWorksInvalidPhone(){
        // Arrange

        validContact = new Contact(3, "Alexandre", "atb@isep.ipp.pt", "2212po34567");

        // Act

        String actualPhoneNumber = validContact.getPhoneNumber();

        // Assert

        assertEquals("Invalid phone number", actualPhoneNumber);
    }

    @Test
    public void seeIfConstructorWorksInvalidPhoneWithoutID(){
        // Arrange

        validContact = new Contact("Alexandre", "atb@isep.ipp.pt", "2212po34567");

        // Act

        String actualPhoneNumber = validContact.getPhoneNumber();

        // Assert

        assertEquals("Invalid phone number", actualPhoneNumber);
    }

    @Test
    public void seeIfSetPhoneNumberWorksInvalidPhone(){
        // Arrange

        validContact = new Contact(3, "Alexandre", "atb@isep.ipp.pt", "221234567");
        validContact.setPhoneNumber("p123p");

        // Act

        String actualPhoneNumber = validContact.getPhoneNumber();

        // Assert

        assertEquals("Invalid phone number", actualPhoneNumber);
    }

    @Test
    public void seeIfConstructorWorksInvalidEmailWithoutID(){
        // Arrange

        validContact = new Contact( "Alexandre", "atbisep.ipp.pt", "221234567");

        // Act

        String actualEmail = validContact.getEmail();

        // Assert

        assertEquals("Invalid e-mail.", actualEmail);
    }

    @Test
    public void seeIfSetEmailWorksInvalidEmail(){
        // Arrange

        validContact = new Contact(3, "Alexandre", "atb@isep.ipp.pt", "221234567");
        validContact.setEmail("tatbadb2");

        // Act

        String actualEmail = validContact.getEmail();

        // Assert

        assertEquals("Invalid e-mail.", actualEmail);
    }
}
