package org.jboss.errai.jpa.client.local;

import com.google.gwt.core.client.UnsafeNativeLong;
import com.google.gwt.json.client.JSONObject;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import javax.persistence.CascadeType;
import javax.persistence.TypedQuery;
import javax.persistence.metamodel.Attribute.PersistentAttributeType;
import javax.persistence.metamodel.Bindable.BindableType;
import javax.persistence.metamodel.ManagedType;
import javax.persistence.metamodel.Type;
import javax.persistence.metamodel.Type.PersistenceType;
import org.atb.errai.demo.contactlist.client.shared.Contact;
import org.jboss.errai.common.client.api.WrappedPortable;
import org.jboss.errai.jpa.client.local.backend.Comparisons;
import org.jboss.errai.jpa.client.local.backend.WebStorageBackend;

public class GeneratedErraiEntityManagerFactory implements ErraiEntityManagerFactory {
  private ErraiEntityType et_org_atb_errai_demo_contactlist_client_shared_Contact;
  public ErraiEntityManager createEntityManager() {
    return new ErraiEntityManager(createMetamodel(), createNamedQueries(), WebStorageBackend.FACTORY);
  }

  private ErraiEntityType createEntityType_org_atb_errai_demo_contactlist_client_shared_Contact() {
    final ErraiEntityType entityType = new ErraiEntityType<Contact>("Contact", Contact.class) {
      public Contact newInstance() {
        return new Contact();
      }

      public void deliverPrePersist(Contact targetEntity) {

      }

      public void deliverPostPersist(Contact targetEntity) {

      }

      public void deliverPreUpdate(Contact targetEntity) {

      }

      public void deliverPostUpdate(Contact targetEntity) {

      }

      public void deliverPreRemove(Contact targetEntity) {

      }

      public void deliverPostRemove(Contact targetEntity) {

      }

      public void deliverPostLoad(Contact targetEntity) {

      }
    };
    entityType.addAttribute(new ErraiSingularAttribute() {
      private ErraiIdGenerator<Long> valueGenerator = new LongIdGenerator(this);
      @UnsafeNativeLong native long Contact_long_id(Contact instance) /*-{
        return instance.@org.atb.errai.demo.contactlist.client.shared.Contact::id;
      }-*/;

      @UnsafeNativeLong native void Contact_long_id(Contact instance, long value) /*-{
        instance.@org.atb.errai.demo.contactlist.client.shared.Contact::id = value;
      }-*/;
      public boolean cascades(CascadeType cascadeType) {
        throw new UnsupportedOperationException("Not a relationship attribute");
      }
      public Object get(Object entityInstance) {
        if (entityInstance instanceof WrappedPortable) {
          entityInstance = ((WrappedPortable) entityInstance).unwrap();
        }
        return Contact_long_id((Contact) entityInstance);
      }
      public Class getBindableJavaType() {
        return long.class;
      }
      public BindableType getBindableType() {
        return BindableType.SINGULAR_ATTRIBUTE;
      }
      public ManagedType getDeclaringType() {
        return et_org_atb_errai_demo_contactlist_client_shared_Contact;
      }
      public Class getJavaType() {
        return long.class;
      }
      public String getName() {
        return "id";
      }
      public PersistentAttributeType getPersistentAttributeType() {
        return PersistentAttributeType.BASIC;
      }
      public Type getType() {
        return new Type() {
          public Class getJavaType() {
            return long.class;
          }
          public PersistenceType getPersistenceType() {
            return PersistenceType.BASIC;
          }
        };
      }
      public ErraiIdGenerator getValueGenerator() {
        return valueGenerator;
      }
      public boolean isAssociation() {
        return false;
      }
      public boolean isCollection() {
        return false;
      }
      public boolean isGeneratedValue() {
        return true;
      }
      public boolean isId() {
        return true;
      }
      public boolean isOptional() {
        return false;
      }
      public boolean isVersion() {
        return false;
      }
      public void set(Object entityInstance, Object value) {
        if (entityInstance instanceof WrappedPortable) {
          entityInstance = ((WrappedPortable) entityInstance).unwrap();
        }
        Contact_long_id((Contact) entityInstance, (Long) value);
      }
    });
    entityType.addAttribute(new ErraiSingularAttribute() {
      native String Contact_String_email(Contact instance) /*-{
        return instance.@org.atb.errai.demo.contactlist.client.shared.Contact::email;
      }-*/;

      native void Contact_String_email(Contact instance, String value) /*-{
        instance.@org.atb.errai.demo.contactlist.client.shared.Contact::email = value;
      }-*/;
      public boolean cascades(CascadeType cascadeType) {
        throw new UnsupportedOperationException("Not a relationship attribute");
      }
      public Object get(Object entityInstance) {
        if (entityInstance instanceof WrappedPortable) {
          entityInstance = ((WrappedPortable) entityInstance).unwrap();
        }
        return Contact_String_email((Contact) entityInstance);
      }
      public Class getBindableJavaType() {
        return String.class;
      }
      public BindableType getBindableType() {
        return BindableType.SINGULAR_ATTRIBUTE;
      }
      public ManagedType getDeclaringType() {
        return et_org_atb_errai_demo_contactlist_client_shared_Contact;
      }
      public Class getJavaType() {
        return String.class;
      }
      public String getName() {
        return "email";
      }
      public PersistentAttributeType getPersistentAttributeType() {
        return PersistentAttributeType.BASIC;
      }
      public Type getType() {
        return new Type() {
          public Class getJavaType() {
            return String.class;
          }
          public PersistenceType getPersistenceType() {
            return PersistenceType.BASIC;
          }
        };
      }
      public ErraiIdGenerator getValueGenerator() {
        throw new UnsupportedOperationException("Not a generated attribute");
      }
      public boolean isAssociation() {
        return false;
      }
      public boolean isCollection() {
        return false;
      }
      public boolean isGeneratedValue() {
        return false;
      }
      public boolean isId() {
        return false;
      }
      public boolean isOptional() {
        return true;
      }
      public boolean isVersion() {
        return false;
      }
      public void set(Object entityInstance, Object value) {
        if (entityInstance instanceof WrappedPortable) {
          entityInstance = ((WrappedPortable) entityInstance).unwrap();
        }
        Contact_String_email((Contact) entityInstance, (String) value);
      }
    });
    entityType.addAttribute(new ErraiSingularAttribute() {
      native String Contact_String_name(Contact instance) /*-{
        return instance.@org.atb.errai.demo.contactlist.client.shared.Contact::name;
      }-*/;

      native void Contact_String_name(Contact instance, String value) /*-{
        instance.@org.atb.errai.demo.contactlist.client.shared.Contact::name = value;
      }-*/;
      public boolean cascades(CascadeType cascadeType) {
        throw new UnsupportedOperationException("Not a relationship attribute");
      }
      public Object get(Object entityInstance) {
        if (entityInstance instanceof WrappedPortable) {
          entityInstance = ((WrappedPortable) entityInstance).unwrap();
        }
        return Contact_String_name((Contact) entityInstance);
      }
      public Class getBindableJavaType() {
        return String.class;
      }
      public BindableType getBindableType() {
        return BindableType.SINGULAR_ATTRIBUTE;
      }
      public ManagedType getDeclaringType() {
        return et_org_atb_errai_demo_contactlist_client_shared_Contact;
      }
      public Class getJavaType() {
        return String.class;
      }
      public String getName() {
        return "name";
      }
      public PersistentAttributeType getPersistentAttributeType() {
        return PersistentAttributeType.BASIC;
      }
      public Type getType() {
        return new Type() {
          public Class getJavaType() {
            return String.class;
          }
          public PersistenceType getPersistenceType() {
            return PersistenceType.BASIC;
          }
        };
      }
      public ErraiIdGenerator getValueGenerator() {
        throw new UnsupportedOperationException("Not a generated attribute");
      }
      public boolean isAssociation() {
        return false;
      }
      public boolean isCollection() {
        return false;
      }
      public boolean isGeneratedValue() {
        return false;
      }
      public boolean isId() {
        return false;
      }
      public boolean isOptional() {
        return true;
      }
      public boolean isVersion() {
        return false;
      }
      public void set(Object entityInstance, Object value) {
        if (entityInstance instanceof WrappedPortable) {
          entityInstance = ((WrappedPortable) entityInstance).unwrap();
        }
        Contact_String_name((Contact) entityInstance, (String) value);
      }
    });
    entityType.addAttribute(new ErraiSingularAttribute() {
      native String Contact_String_phoneNumber(Contact instance) /*-{
        return instance.@org.atb.errai.demo.contactlist.client.shared.Contact::phoneNumber;
      }-*/;

      native void Contact_String_phoneNumber(Contact instance, String value) /*-{
        instance.@org.atb.errai.demo.contactlist.client.shared.Contact::phoneNumber = value;
      }-*/;
      public boolean cascades(CascadeType cascadeType) {
        throw new UnsupportedOperationException("Not a relationship attribute");
      }
      public Object get(Object entityInstance) {
        if (entityInstance instanceof WrappedPortable) {
          entityInstance = ((WrappedPortable) entityInstance).unwrap();
        }
        return Contact_String_phoneNumber((Contact) entityInstance);
      }
      public Class getBindableJavaType() {
        return String.class;
      }
      public BindableType getBindableType() {
        return BindableType.SINGULAR_ATTRIBUTE;
      }
      public ManagedType getDeclaringType() {
        return et_org_atb_errai_demo_contactlist_client_shared_Contact;
      }
      public Class getJavaType() {
        return String.class;
      }
      public String getName() {
        return "phoneNumber";
      }
      public PersistentAttributeType getPersistentAttributeType() {
        return PersistentAttributeType.BASIC;
      }
      public Type getType() {
        return new Type() {
          public Class getJavaType() {
            return String.class;
          }
          public PersistenceType getPersistenceType() {
            return PersistenceType.BASIC;
          }
        };
      }
      public ErraiIdGenerator getValueGenerator() {
        throw new UnsupportedOperationException("Not a generated attribute");
      }
      public boolean isAssociation() {
        return false;
      }
      public boolean isCollection() {
        return false;
      }
      public boolean isGeneratedValue() {
        return false;
      }
      public boolean isId() {
        return false;
      }
      public boolean isOptional() {
        return true;
      }
      public boolean isVersion() {
        return false;
      }
      public void set(Object entityInstance, Object value) {
        if (entityInstance instanceof WrappedPortable) {
          entityInstance = ((WrappedPortable) entityInstance).unwrap();
        }
        Contact_String_phoneNumber((Contact) entityInstance, (String) value);
      }
    });
    return entityType;
  }

  private ErraiMetamodel createMetamodel() {
    ErraiMetamodel metamodel = new ErraiMetamodel();
    // **
    // ** EntityType for org.atb.errai.demo.contactlist.client.shared.Contact
    // **
    et_org_atb_errai_demo_contactlist_client_shared_Contact = createEntityType_org_atb_errai_demo_contactlist_client_shared_Contact();
    et_org_atb_errai_demo_contactlist_client_shared_Contact.addSubtype(et_org_atb_errai_demo_contactlist_client_shared_Contact);
    metamodel.addEntityType(et_org_atb_errai_demo_contactlist_client_shared_Contact);
    metamodel.freeze();
    return metamodel;
  }

  protected Map createNamedQueries() {
    final Map namedQueries = new HashMap();
    // **
    // ** NamedQuery "allContacts"
    // ** SELECT c FROM Contact c ORDER BY c.id
    // **
    namedQueries.put("allContacts", new TypedQueryFactory(Contact.class, new ErraiParameter[] { }) {
      protected TypedQuery createQuery(final ErraiEntityManager entityManager) {
        return new ErraiTypedQuery(entityManager, actualResultType, parameters) {
          protected Comparator getComparator() {
            return new Comparator() {
              private final ErraiAttribute c_id_attr = entityManager.getMetamodel().entity(Contact.class).getAttribute("id");
              public int compare(Object o1, Object o2) {
                final Contact lhs = (Contact) o1;
                final Contact rhs = (Contact) o2;
                int result;
                result = Comparisons.nullSafeCompare((Comparable) c_id_attr.get(lhs), (Comparable) c_id_attr.get(rhs));
                if (result != 0) {
                  return + result;
                }
                return 0;
              }
            };
          }
          public boolean matches(JSONObject candidate) {
            return true;
          }
        };
      }
    });
    return namedQueries;
  }
}