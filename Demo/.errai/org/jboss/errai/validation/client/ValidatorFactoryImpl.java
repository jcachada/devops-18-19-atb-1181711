package org.jboss.errai.validation.client;

import com.google.gwt.validation.client.AbstractGwtValidatorFactory;
import com.google.gwt.validation.client.impl.AbstractGwtValidator;

public class ValidatorFactoryImpl extends AbstractGwtValidatorFactory { public AbstractGwtValidator createValidator() {
    return new BeanValidator(null);
  }
}