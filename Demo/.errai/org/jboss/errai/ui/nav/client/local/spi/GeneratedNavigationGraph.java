package org.jboss.errai.ui.nav.client.local.spi;

import java.util.HashMap;
import java.util.Map;
import org.atb.errai.demo.contactlist.client.local.ContactListPage;
import org.atb.errai.demo.contactlist.client.local.NewContactPage;
import org.jboss.errai.common.client.PageRequest;
import org.jboss.errai.common.client.util.CreationalCallback;
import org.jboss.errai.enterprise.client.cdi.api.CDI;
import org.jboss.errai.ui.nav.client.local.DefaultPage;
import org.jboss.errai.ui.nav.client.local.HistoryToken;
import org.jboss.errai.ui.nav.client.local.api.NavigationControl;
import org.jboss.errai.ui.nav.client.shared.NavigationEvent;

public class GeneratedNavigationGraph extends NavigationGraph { public GeneratedNavigationGraph() {
    final PageNode newContactPage = new PageNode<NewContactPage>() {
      public String name() {
        return "NewContactPage";
      }

      public String toString() {
        return "NewContactPage";
      }

      public String getURL() {
        return "NewContactPage";
      }

      public Class contentType() {
        return NewContactPage.class;
      }

      public void produceContent(CreationalCallback callback) {
        bm.lookupBean(NewContactPage.class).getInstance(callback);
      }

      public void pageHiding(NewContactPage widget, NavigationControl control) {
        control.proceed();
      }

      public void pageHidden(NewContactPage widget) {

      }

      public void pageShowing(NewContactPage widget, HistoryToken state, NavigationControl control) {
        final Map pageState = new HashMap() {
          {

          }
        };
        CDI.fireEvent(new NavigationEvent(new PageRequest("NewContactPage", pageState)));
        control.proceed();
      }

      public void pageShown(NewContactPage widget, HistoryToken state) {
        final Map pageState = new HashMap() {
          {

          }
        };
      }

      public void destroy(NewContactPage widget) {
        bm.destroyBean(widget);
      }
    };
    pagesByName.put("NewContactPage", newContactPage);
    final PageNode defaultPage = new PageNode<ContactListPage>() {
      public String name() {
        return "ContactListPage";
      }

      public String toString() {
        return "ContactListPage";
      }

      public String getURL() {
        return "ContactListPage";
      }

      public Class contentType() {
        return ContactListPage.class;
      }

      public void produceContent(CreationalCallback callback) {
        bm.lookupBean(ContactListPage.class).getInstance(callback);
      }

      public void pageHiding(ContactListPage widget, NavigationControl control) {
        control.proceed();
      }

      public void pageHidden(ContactListPage widget) {

      }

      public void pageShowing(ContactListPage widget, HistoryToken state, NavigationControl control) {
        final Map pageState = new HashMap() {
          {

          }
        };
        CDI.fireEvent(new NavigationEvent(new PageRequest("ContactListPage", pageState)));
        control.proceed();
      }

      public void pageShown(ContactListPage widget, HistoryToken state) {
        final Map pageState = new HashMap() {
          {

          }
        };
      }

      public void destroy(ContactListPage widget) {
        bm.destroyBean(widget);
      }
    };
    pagesByName.put("", defaultPage);
    pagesByRole.put(DefaultPage.class, defaultPage);
    final PageNode contactListPage = defaultPage;
    pagesByName.put("ContactListPage", contactListPage);
  }

}