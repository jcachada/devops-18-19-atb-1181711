package org.jboss.errai.enterprise.client.jaxrs;

import com.google.gwt.http.client.RequestBuilder;
import com.google.gwt.http.client.Response;
import java.util.List;
import org.atb.errai.demo.contactlist.client.shared.Contact;
import org.atb.errai.demo.contactlist.client.shared.ContactService;
import org.jboss.errai.common.client.api.ErrorCallback;
import org.jboss.errai.common.client.api.RemoteCallback;
import org.jboss.errai.common.client.framework.ProxyProvider;
import org.jboss.errai.common.client.framework.RemoteServiceProxyFactory;

public class JaxrsProxyLoaderImpl implements JaxrsProxyLoader { public void loadProxies() {
    class org_atb_errai_demo_contactlist_client_shared_ContactServiceImpl extends AbstractJaxrsProxy implements ContactService {
      private RemoteCallback remoteCallback;
      private ErrorCallback errorCallback;
      public org_atb_errai_demo_contactlist_client_shared_ContactServiceImpl() {

      }

      public RemoteCallback getRemoteCallback() {
        return remoteCallback;
      }

      public void setRemoteCallback(RemoteCallback callback) {
        remoteCallback = callback;
      }

      public ErrorCallback getErrorCallback() {
        return errorCallback;
      }

      public void setErrorCallback(ErrorCallback callback) {
        errorCallback = callback;
      }

      public List listAllContacts() {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("rest/contacts");
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.GET, url.toString());
        requestBuilder.setHeader("Accept", "application/json");
        sendRequest(requestBuilder, null, new ResponseDemarshallingCallback() {
          public Object demarshallResponse(Response response) {
            if (response.getStatusCode() == 204) {
              return null;
            } else {
              return MarshallingWrapper.fromJSON(response.getText(), List.class, Contact.class);
            }
          }
        });
        return null;
      }

      public javax.ws.rs.core.Response createContact(final Contact a0) {
        StringBuilder url = new StringBuilder(getBaseUrl());
        url.append("rest/contacts");
        RequestBuilder requestBuilder = new RequestBuilder(RequestBuilder.POST, url.toString());
        requestBuilder.setHeader("Accept", "text/plain");
        requestBuilder.setHeader("Content-Type", "application/json");
        sendRequest(requestBuilder, MarshallingWrapper.toJSON(a0), new ResponseDemarshallingCallback() {
          public Object demarshallResponse(Response response) {
            return response;
          }
        });
        return null;
      }
    }
    RemoteServiceProxyFactory.addRemoteProxy(ContactService.class, new ProxyProvider() {
      public Object getProxy() {
        return new org_atb_errai_demo_contactlist_client_shared_ContactServiceImpl();
      }
    });
  }
}