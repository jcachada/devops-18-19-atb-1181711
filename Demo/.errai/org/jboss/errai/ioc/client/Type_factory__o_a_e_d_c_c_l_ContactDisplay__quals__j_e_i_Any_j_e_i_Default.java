package org.jboss.errai.ioc.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.user.client.TakesValue;
import com.google.gwt.user.client.ui.Widget;
import elemental2.dom.HTMLDivElement;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;
import javax.enterprise.context.Dependent;
import org.atb.errai.demo.contactlist.client.local.ContactDisplay;
import org.atb.errai.demo.contactlist.client.shared.Contact;
import org.jboss.errai.common.client.ui.ElementWrapperWidget;
import org.jboss.errai.databinding.client.BoundUtil;
import org.jboss.errai.databinding.client.api.DataBinder;
import org.jboss.errai.ioc.client.container.ContextManager;
import org.jboss.errai.ioc.client.container.Factory;
import org.jboss.errai.ioc.client.container.FactoryHandleImpl;
import org.jboss.errai.ui.client.local.api.IsElement;
import org.jboss.errai.ui.shared.DataFieldMeta;
import org.jboss.errai.ui.shared.Template;
import org.jboss.errai.ui.shared.TemplateUtil;
import org.jboss.errai.ui.shared.api.style.StyleBindingsRegistry;

public class Type_factory__o_a_e_d_c_c_l_ContactDisplay__quals__j_e_i_Any_j_e_i_Default extends Factory<ContactDisplay> { public interface o_a_e_d_c_c_l_ContactDisplayTemplateResource extends Template, ClientBundle { @Source("org/atb/errai/demo/contactlist/client/local/ContactListPage.html") public TextResource getContents(); }
  public Type_factory__o_a_e_d_c_c_l_ContactDisplay__quals__j_e_i_Any_j_e_i_Default() {
    super(new FactoryHandleImpl(ContactDisplay.class, "Type_factory__o_a_e_d_c_c_l_ContactDisplay__quals__j_e_i_Any_j_e_i_Default", Dependent.class, false, null, true));
    handle.setAssignableTypes(new Class[] { ContactDisplay.class, Object.class, TakesValue.class, IsElement.class, org.jboss.errai.common.client.api.IsElement.class });
  }

  public ContactDisplay createInstance(final ContextManager contextManager) {
    final ContactDisplay instance = new ContactDisplay();
    setIncompleteInstance(instance);
    final HTMLDivElement ContactDisplay_phoneNumber = (HTMLDivElement) contextManager.getInstance("ExtensionProvided_factory__e_d_HTMLDivElement__quals__j_e_i_Any_j_e_i_Default_j_i_Named");
    registerDependentScopedReference(instance, ContactDisplay_phoneNumber);
    ContactDisplay_HTMLDivElement_phoneNumber(instance, ContactDisplay_phoneNumber);
    final DataBinder ContactDisplay_binder = (DataBinder) contextManager.getContextualInstance("ContextualProvider_factory__o_j_e_d_c_a_DataBinder__quals__Universal", new Class[] { Contact.class }, new Annotation[] { });
    registerDependentScopedReference(instance, ContactDisplay_binder);
    ContactDisplay_DataBinder_binder(instance, ContactDisplay_binder);
    final HTMLDivElement ContactDisplay_email = (HTMLDivElement) contextManager.getInstance("ExtensionProvided_factory__e_d_HTMLDivElement__quals__j_e_i_Any_j_e_i_Default_j_i_Named");
    registerDependentScopedReference(instance, ContactDisplay_email);
    ContactDisplay_HTMLDivElement_email(instance, ContactDisplay_email);
    final HTMLDivElement ContactDisplay_name = (HTMLDivElement) contextManager.getInstance("ExtensionProvided_factory__e_d_HTMLDivElement__quals__j_e_i_Any_j_e_i_Default_j_i_Named");
    registerDependentScopedReference(instance, ContactDisplay_name);
    ContactDisplay_HTMLDivElement_name(instance, ContactDisplay_name);
    o_a_e_d_c_c_l_ContactDisplayTemplateResource templateForContactDisplay = GWT.create(o_a_e_d_c_c_l_ContactDisplayTemplateResource.class);
    Element parentElementForTemplateOfContactDisplay = TemplateUtil.getRootTemplateParentElement(templateForContactDisplay.getContents().getText(), "org/atb/errai/demo/contactlist/client/local/ContactListPage.html", "contact");
    TemplateUtil.translateTemplate("org/atb/errai/demo/contactlist/client/local/ContactListPage.html", TemplateUtil.getRootTemplateElement(parentElementForTemplateOfContactDisplay));
    Map<String, Element> dataFieldElements = TemplateUtil.getDataFieldElements(TemplateUtil.getRootTemplateElement(parentElementForTemplateOfContactDisplay));
    final Map<String, DataFieldMeta> dataFieldMetas = new HashMap<String, DataFieldMeta>(3);
    dataFieldMetas.put("name", new DataFieldMeta());
    dataFieldMetas.put("email", new DataFieldMeta());
    dataFieldMetas.put("phoneNumber", new DataFieldMeta());
    Map<String, Widget> templateFieldsMap = new LinkedHashMap<String, Widget>();
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.ContactDisplay", "org/atb/errai/demo/contactlist/client/local/ContactListPage.html", new Supplier<Widget>() {
      public Widget get() {
        return ElementWrapperWidget.getWidget(TemplateUtil.asElement(ContactDisplay_HTMLDivElement_name(instance)));
      }
    }, dataFieldElements, dataFieldMetas, "name");
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.ContactDisplay", "org/atb/errai/demo/contactlist/client/local/ContactListPage.html", new Supplier<Widget>() {
      public Widget get() {
        return ElementWrapperWidget.getWidget(TemplateUtil.asElement(ContactDisplay_HTMLDivElement_email(instance)));
      }
    }, dataFieldElements, dataFieldMetas, "email");
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.ContactDisplay", "org/atb/errai/demo/contactlist/client/local/ContactListPage.html", new Supplier<Widget>() {
      public Widget get() {
        return ElementWrapperWidget.getWidget(TemplateUtil.asElement(ContactDisplay_HTMLDivElement_phoneNumber(instance)));
      }
    }, dataFieldElements, dataFieldMetas, "phoneNumber");
    templateFieldsMap.put("name", ElementWrapperWidget.getWidget(TemplateUtil.asElement(ContactDisplay_HTMLDivElement_name(instance))));
    templateFieldsMap.put("email", ElementWrapperWidget.getWidget(TemplateUtil.asElement(ContactDisplay_HTMLDivElement_email(instance))));
    templateFieldsMap.put("phoneNumber", ElementWrapperWidget.getWidget(TemplateUtil.asElement(ContactDisplay_HTMLDivElement_phoneNumber(instance))));
    TemplateUtil.initTemplated(instance, TemplateUtil.getRootTemplateElement(parentElementForTemplateOfContactDisplay), templateFieldsMap.values());
    DataBinder binder = ContactDisplay_DataBinder_binder(instance);
    if (binder == null) {
      throw new RuntimeException("@AutoBound data binder for class org.atb.errai.demo.contactlist.client.local.ContactDisplay has not been initialized. Either initialize or add @Inject!");
    }
    thisInstance.setReference(instance, "DataModelBinder", binder);
    binder.bind(ElementWrapperWidget.getWidget(BoundUtil.asElement(ContactDisplay_HTMLDivElement_name(instance))), "name", null, null, false);
    binder.bind(ElementWrapperWidget.getWidget(BoundUtil.asElement(ContactDisplay_HTMLDivElement_email(instance))), "email", null, null, false);
    binder.bind(ElementWrapperWidget.getWidget(BoundUtil.asElement(ContactDisplay_HTMLDivElement_phoneNumber(instance))), "phoneNumber", null, null, false);
    StyleBindingsRegistry.get().updateStyles(instance);
    setIncompleteInstance(null);
    return instance;
  }

  public void generatedDestroyInstance(final Object instance, final ContextManager contextManager) {
    destroyInstanceHelper((ContactDisplay) instance, contextManager);
  }

  public void destroyInstanceHelper(final ContactDisplay instance, final ContextManager contextManager) {
    TemplateUtil.cleanupTemplated(instance);
    ((DataBinder) thisInstance.getReferenceAs(instance, "DataModelBinder", DataBinder.class)).unbind();
  }

  native static HTMLDivElement ContactDisplay_HTMLDivElement_name(ContactDisplay instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.ContactDisplay::name;
  }-*/;

  native static void ContactDisplay_HTMLDivElement_name(ContactDisplay instance, HTMLDivElement value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.ContactDisplay::name = value;
  }-*/;

  native static HTMLDivElement ContactDisplay_HTMLDivElement_email(ContactDisplay instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.ContactDisplay::email;
  }-*/;

  native static void ContactDisplay_HTMLDivElement_email(ContactDisplay instance, HTMLDivElement value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.ContactDisplay::email = value;
  }-*/;

  native static DataBinder ContactDisplay_DataBinder_binder(ContactDisplay instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.ContactDisplay::binder;
  }-*/;

  native static void ContactDisplay_DataBinder_binder(ContactDisplay instance, DataBinder<Contact> value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.ContactDisplay::binder = value;
  }-*/;

  native static HTMLDivElement ContactDisplay_HTMLDivElement_phoneNumber(ContactDisplay instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.ContactDisplay::phoneNumber;
  }-*/;

  native static void ContactDisplay_HTMLDivElement_phoneNumber(ContactDisplay instance, HTMLDivElement value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.ContactDisplay::phoneNumber = value;
  }-*/;
}