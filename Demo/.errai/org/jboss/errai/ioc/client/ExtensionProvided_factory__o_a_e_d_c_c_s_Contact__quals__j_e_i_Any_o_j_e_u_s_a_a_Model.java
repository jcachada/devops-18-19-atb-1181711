package org.jboss.errai.ioc.client;

import java.io.Serializable;
import java.lang.annotation.Annotation;
import javax.enterprise.context.Dependent;
import org.atb.errai.demo.contactlist.client.shared.Contact;
import org.jboss.errai.databinding.client.api.DataBinder;
import org.jboss.errai.ioc.client.container.ContextManager;
import org.jboss.errai.ioc.client.container.Factory;
import org.jboss.errai.ioc.client.container.FactoryHandleImpl;
import org.jboss.errai.ui.shared.api.annotations.Model;

public class ExtensionProvided_factory__o_a_e_d_c_c_s_Contact__quals__j_e_i_Any_o_j_e_u_s_a_a_Model extends Factory<Contact> { public ExtensionProvided_factory__o_a_e_d_c_c_s_Contact__quals__j_e_i_Any_o_j_e_u_s_a_a_Model() {
    super(new FactoryHandleImpl(Contact.class, "ExtensionProvided_factory__o_a_e_d_c_c_s_Contact__quals__j_e_i_Any_o_j_e_u_s_a_a_Model", Dependent.class, false, null, true));
    handle.setAssignableTypes(new Class[] { Contact.class, Object.class, Serializable.class, Comparable.class });
    handle.setQualifiers(new Annotation[] { QualifierUtil.ANY_ANNOTATION, new Model() {
        public Class annotationType() {
          return Model.class;
        }
    } });
  }

  public Contact createInstance(final ContextManager contextManager) {
    final DataBinder<Contact> dataBinder = DataBinder.forType(Contact.class);
    final Contact model = dataBinder.getModel();
    setReference(model, "DataModelBinder", dataBinder);
    return model;
  }
}