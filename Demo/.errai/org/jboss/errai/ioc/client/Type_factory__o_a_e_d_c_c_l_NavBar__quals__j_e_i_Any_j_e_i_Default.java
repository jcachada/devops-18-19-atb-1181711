package org.jboss.errai.ioc.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasAttachHandlers;
import com.google.gwt.event.shared.HasHandlers;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.IsRenderable;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;
import javax.enterprise.context.Dependent;
import org.atb.errai.demo.contactlist.client.local.ContactListPage;
import org.atb.errai.demo.contactlist.client.local.NavBar;
import org.jboss.errai.ioc.client.container.ContextManager;
import org.jboss.errai.ioc.client.container.Factory;
import org.jboss.errai.ioc.client.container.FactoryHandleImpl;
import org.jboss.errai.ui.nav.client.local.TransitionTo;
import org.jboss.errai.ui.shared.DataFieldMeta;
import org.jboss.errai.ui.shared.Template;
import org.jboss.errai.ui.shared.TemplateUtil;
import org.jboss.errai.ui.shared.api.style.StyleBindingsRegistry;

public class Type_factory__o_a_e_d_c_c_l_NavBar__quals__j_e_i_Any_j_e_i_Default extends Factory<NavBar> { public interface o_a_e_d_c_c_l_NavBarTemplateResource extends Template, ClientBundle { @Source("org/atb/errai/demo/contactlist/client/local/NavBar.html") public TextResource getContents(); }
  public Type_factory__o_a_e_d_c_c_l_NavBar__quals__j_e_i_Any_j_e_i_Default() {
    super(new FactoryHandleImpl(NavBar.class, "Type_factory__o_a_e_d_c_c_l_NavBar__quals__j_e_i_Any_j_e_i_Default", Dependent.class, false, null, true));
    handle.setAssignableTypes(new Class[] { NavBar.class, Composite.class, Widget.class, UIObject.class, Object.class, HasVisibility.class, EventListener.class, HasAttachHandlers.class, HasHandlers.class, IsWidget.class, IsRenderable.class });
  }

  public NavBar createInstance(final ContextManager contextManager) {
    final NavBar instance = new NavBar();
    setIncompleteInstance(instance);
    final Button NavBar_info = (Button) contextManager.getInstance("ExtensionProvided_factory__c_g_g_u_c_u_Button__quals__j_e_i_Any_j_e_i_Default");
    registerDependentScopedReference(instance, NavBar_info);
    NavBar_Button_info(instance, NavBar_info);
    final TransitionTo NavBar_homeTab = (TransitionTo) contextManager.getContextualInstance("ContextualProvider_factory__o_j_e_u_n_c_l_TransitionTo__quals__Universal", new Class[] { ContactListPage.class }, new Annotation[] { });
    registerDependentScopedReference(instance, NavBar_homeTab);
    NavBar_TransitionTo_homeTab(instance, NavBar_homeTab);
    o_a_e_d_c_c_l_NavBarTemplateResource templateForNavBar = GWT.create(o_a_e_d_c_c_l_NavBarTemplateResource.class);
    Element parentElementForTemplateOfNavBar = TemplateUtil.getRootTemplateParentElement(templateForNavBar.getContents().getText(), "org/atb/errai/demo/contactlist/client/local/NavBar.html", "");
    TemplateUtil.translateTemplate("org/atb/errai/demo/contactlist/client/local/NavBar.html", TemplateUtil.getRootTemplateElement(parentElementForTemplateOfNavBar));
    Map<String, Element> dataFieldElements = TemplateUtil.getDataFieldElements(TemplateUtil.getRootTemplateElement(parentElementForTemplateOfNavBar));
    final Map<String, DataFieldMeta> dataFieldMetas = new HashMap<String, DataFieldMeta>(1);
    dataFieldMetas.put("info", new DataFieldMeta());
    Map<String, Widget> templateFieldsMap = new LinkedHashMap<String, Widget>();
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.NavBar", "org/atb/errai/demo/contactlist/client/local/NavBar.html", new Supplier<Widget>() {
      public Widget get() {
        return NavBar_Button_info(instance).asWidget();
      }
    }, dataFieldElements, dataFieldMetas, "info");
    templateFieldsMap.put("info", NavBar_Button_info(instance).asWidget());
    TemplateUtil.initWidget(instance, TemplateUtil.getRootTemplateElement(parentElementForTemplateOfNavBar), templateFieldsMap.values());
    ((HasClickHandlers) templateFieldsMap.get("info")).addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        instance.onHomeButtonClick(event);
      }
    });
    StyleBindingsRegistry.get().updateStyles(instance);
    setIncompleteInstance(null);
    return instance;
  }

  public void generatedDestroyInstance(final Object instance, final ContextManager contextManager) {
    destroyInstanceHelper((NavBar) instance, contextManager);
  }

  public void destroyInstanceHelper(final NavBar instance, final ContextManager contextManager) {
    TemplateUtil.cleanupWidget(instance);
  }

  native static Button NavBar_Button_info(NavBar instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.NavBar::info;
  }-*/;

  native static void NavBar_Button_info(NavBar instance, Button value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.NavBar::info = value;
  }-*/;

  native static TransitionTo NavBar_TransitionTo_homeTab(NavBar instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.NavBar::homeTab;
  }-*/;

  native static void NavBar_TransitionTo_homeTab(NavBar instance, TransitionTo<ContactListPage> value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.NavBar::homeTab = value;
  }-*/;
}