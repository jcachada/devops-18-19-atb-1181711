package org.jboss.errai.ioc.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.event.logical.shared.HasAttachHandlers;
import com.google.gwt.event.shared.HasHandlers;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.IsRenderable;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;
import javax.enterprise.context.Dependent;
import org.atb.errai.demo.contactlist.client.local.ContactListPage;
import org.atb.errai.demo.contactlist.client.local.NewContactPage;
import org.atb.errai.demo.contactlist.client.shared.Contact;
import org.atb.errai.demo.contactlist.client.shared.ContactService;
import org.jboss.errai.common.client.api.Caller;
import org.jboss.errai.databinding.client.api.Convert;
import org.jboss.errai.databinding.client.api.DataBinder;
import org.jboss.errai.ioc.client.container.ContextManager;
import org.jboss.errai.ioc.client.container.Factory;
import org.jboss.errai.ioc.client.container.FactoryHandleImpl;
import org.jboss.errai.ui.nav.client.local.TransitionTo;
import org.jboss.errai.ui.shared.DataFieldMeta;
import org.jboss.errai.ui.shared.Template;
import org.jboss.errai.ui.shared.TemplateUtil;
import org.jboss.errai.ui.shared.api.style.StyleBindingsRegistry;

public class Type_factory__o_a_e_d_c_c_l_NewContactPage__quals__j_e_i_Any_j_e_i_Default extends Factory<NewContactPage> { public interface o_a_e_d_c_c_l_NewContactPageTemplateResource extends Template, ClientBundle { @Source("org/atb/errai/demo/contactlist/client/local/NewContactPage.html") public TextResource getContents(); }
  public Type_factory__o_a_e_d_c_c_l_NewContactPage__quals__j_e_i_Any_j_e_i_Default() {
    super(new FactoryHandleImpl(NewContactPage.class, "Type_factory__o_a_e_d_c_c_l_NewContactPage__quals__j_e_i_Any_j_e_i_Default", Dependent.class, false, null, true));
    handle.setAssignableTypes(new Class[] { NewContactPage.class, Composite.class, Widget.class, UIObject.class, Object.class, HasVisibility.class, EventListener.class, HasAttachHandlers.class, HasHandlers.class, IsWidget.class, IsRenderable.class });
  }

  public NewContactPage createInstance(final ContextManager contextManager) {
    final NewContactPage instance = new NewContactPage();
    setIncompleteInstance(instance);
    final TextBox NewContactPage_name = (TextBox) contextManager.getInstance("ExtensionProvided_factory__c_g_g_u_c_u_TextBox__quals__j_e_i_Any_j_e_i_Default");
    registerDependentScopedReference(instance, NewContactPage_name);
    NewContactPage_TextBox_name(instance, NewContactPage_name);
    final TransitionTo NewContactPage_goToContactListPage = (TransitionTo) contextManager.getContextualInstance("ContextualProvider_factory__o_j_e_u_n_c_l_TransitionTo__quals__Universal", new Class[] { ContactListPage.class }, new Annotation[] { });
    registerDependentScopedReference(instance, NewContactPage_goToContactListPage);
    NewContactPage_TransitionTo_goToContactListPage(instance, NewContactPage_goToContactListPage);
    final TextBox NewContactPage_email = (TextBox) contextManager.getInstance("ExtensionProvided_factory__c_g_g_u_c_u_TextBox__quals__j_e_i_Any_j_e_i_Default");
    registerDependentScopedReference(instance, NewContactPage_email);
    NewContactPage_TextBox_email(instance, NewContactPage_email);
    final Button NewContactPage_create = (Button) contextManager.getInstance("ExtensionProvided_factory__c_g_g_u_c_u_Button__quals__j_e_i_Any_j_e_i_Default");
    registerDependentScopedReference(instance, NewContactPage_create);
    NewContactPage_Button_create(instance, NewContactPage_create);
    final Caller NewContactPage_contactService = (Caller) contextManager.getContextualInstance("ContextualProvider_factory__o_j_e_c_c_a_Caller__quals__Universal", new Class[] { ContactService.class }, new Annotation[] { });
    registerDependentScopedReference(instance, NewContactPage_contactService);
    NewContactPage_Caller_contactService(instance, NewContactPage_contactService);
    final TextBox NewContactPage_phoneNumber = (TextBox) contextManager.getInstance("ExtensionProvided_factory__c_g_g_u_c_u_TextBox__quals__j_e_i_Any_j_e_i_Default");
    registerDependentScopedReference(instance, NewContactPage_phoneNumber);
    NewContactPage_TextBox_phoneNumber(instance, NewContactPage_phoneNumber);
    final Contact NewContactPage_contact = (Contact) contextManager.getInstance("ExtensionProvided_factory__o_a_e_d_c_c_s_Contact__quals__j_e_i_Any_o_j_e_u_s_a_a_Model");
    registerDependentScopedReference(instance, NewContactPage_contact);
    NewContactPage_Contact_contact(instance, NewContactPage_contact);
    o_a_e_d_c_c_l_NewContactPageTemplateResource templateForNewContactPage = GWT.create(o_a_e_d_c_c_l_NewContactPageTemplateResource.class);
    Element parentElementForTemplateOfNewContactPage = TemplateUtil.getRootTemplateParentElement(templateForNewContactPage.getContents().getText(), "org/atb/errai/demo/contactlist/client/local/NewContactPage.html", "form-newcontact");
    TemplateUtil.translateTemplate("org/atb/errai/demo/contactlist/client/local/NewContactPage.html", TemplateUtil.getRootTemplateElement(parentElementForTemplateOfNewContactPage));
    Map<String, Element> dataFieldElements = TemplateUtil.getDataFieldElements(TemplateUtil.getRootTemplateElement(parentElementForTemplateOfNewContactPage));
    final Map<String, DataFieldMeta> dataFieldMetas = new HashMap<String, DataFieldMeta>(4);
    dataFieldMetas.put("name", new DataFieldMeta());
    dataFieldMetas.put("email", new DataFieldMeta());
    dataFieldMetas.put("phoneNumber", new DataFieldMeta());
    dataFieldMetas.put("create", new DataFieldMeta());
    Map<String, Widget> templateFieldsMap = new LinkedHashMap<String, Widget>();
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.NewContactPage", "org/atb/errai/demo/contactlist/client/local/NewContactPage.html", new Supplier<Widget>() {
      public Widget get() {
        return NewContactPage_TextBox_name(instance).asWidget();
      }
    }, dataFieldElements, dataFieldMetas, "name");
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.NewContactPage", "org/atb/errai/demo/contactlist/client/local/NewContactPage.html", new Supplier<Widget>() {
      public Widget get() {
        return NewContactPage_TextBox_email(instance).asWidget();
      }
    }, dataFieldElements, dataFieldMetas, "email");
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.NewContactPage", "org/atb/errai/demo/contactlist/client/local/NewContactPage.html", new Supplier<Widget>() {
      public Widget get() {
        return NewContactPage_TextBox_phoneNumber(instance).asWidget();
      }
    }, dataFieldElements, dataFieldMetas, "phoneNumber");
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.NewContactPage", "org/atb/errai/demo/contactlist/client/local/NewContactPage.html", new Supplier<Widget>() {
      public Widget get() {
        return NewContactPage_Button_create(instance).asWidget();
      }
    }, dataFieldElements, dataFieldMetas, "create");
    templateFieldsMap.put("name", NewContactPage_TextBox_name(instance).asWidget());
    templateFieldsMap.put("email", NewContactPage_TextBox_email(instance).asWidget());
    templateFieldsMap.put("phoneNumber", NewContactPage_TextBox_phoneNumber(instance).asWidget());
    templateFieldsMap.put("create", NewContactPage_Button_create(instance).asWidget());
    TemplateUtil.initWidget(instance, TemplateUtil.getRootTemplateElement(parentElementForTemplateOfNewContactPage), templateFieldsMap.values());
    ((HasClickHandlers) templateFieldsMap.get("create")).addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        instance.createContact(event);
      }
    });
    DataBinder binder = (DataBinder) contextManager.getInstanceProperty(NewContactPage_Contact_contact(instance), "DataModelBinder", DataBinder.class);
    if (binder == null) {
      throw new RuntimeException("@AutoBound data binder for class org.atb.errai.demo.contactlist.client.local.NewContactPage has not been initialized. Either initialize or add @Inject!");
    }
    thisInstance.setReference(instance, "DataModelBinder", binder);
    binder.bind(NewContactPage_TextBox_name(instance), "name", Convert.getConverter(String.class, String.class), null, false);
    binder.bind(NewContactPage_TextBox_email(instance), "email", Convert.getConverter(String.class, String.class), null, false);
    binder.bind(NewContactPage_TextBox_phoneNumber(instance), "phoneNumber", Convert.getConverter(String.class, String.class), null, false);
    StyleBindingsRegistry.get().updateStyles(instance);
    setIncompleteInstance(null);
    return instance;
  }

  public void generatedDestroyInstance(final Object instance, final ContextManager contextManager) {
    destroyInstanceHelper((NewContactPage) instance, contextManager);
  }

  public void destroyInstanceHelper(final NewContactPage instance, final ContextManager contextManager) {
    TemplateUtil.cleanupWidget(instance);
    ((DataBinder) thisInstance.getReferenceAs(instance, "DataModelBinder", DataBinder.class)).unbind();
  }

  native static Contact NewContactPage_Contact_contact(NewContactPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::contact;
  }-*/;

  native static void NewContactPage_Contact_contact(NewContactPage instance, Contact value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::contact = value;
  }-*/;

  native static Caller NewContactPage_Caller_contactService(NewContactPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::contactService;
  }-*/;

  native static void NewContactPage_Caller_contactService(NewContactPage instance, Caller<ContactService> value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::contactService = value;
  }-*/;

  native static TextBox NewContactPage_TextBox_name(NewContactPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::name;
  }-*/;

  native static void NewContactPage_TextBox_name(NewContactPage instance, TextBox value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::name = value;
  }-*/;

  native static TextBox NewContactPage_TextBox_email(NewContactPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::email;
  }-*/;

  native static void NewContactPage_TextBox_email(NewContactPage instance, TextBox value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::email = value;
  }-*/;

  native static TextBox NewContactPage_TextBox_phoneNumber(NewContactPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::phoneNumber;
  }-*/;

  native static void NewContactPage_TextBox_phoneNumber(NewContactPage instance, TextBox value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::phoneNumber = value;
  }-*/;

  native static TransitionTo NewContactPage_TransitionTo_goToContactListPage(NewContactPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::goToContactListPage;
  }-*/;

  native static void NewContactPage_TransitionTo_goToContactListPage(NewContactPage instance, TransitionTo<ContactListPage> value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::goToContactListPage = value;
  }-*/;

  native static Button NewContactPage_Button_create(NewContactPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::create;
  }-*/;

  native static void NewContactPage_Button_create(NewContactPage instance, Button value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.NewContactPage::create = value;
  }-*/;
}