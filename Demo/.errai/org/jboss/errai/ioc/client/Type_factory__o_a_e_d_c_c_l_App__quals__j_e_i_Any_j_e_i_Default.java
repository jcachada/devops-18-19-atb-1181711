package org.jboss.errai.ioc.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.logical.shared.HasAttachHandlers;
import com.google.gwt.event.shared.HasHandlers;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.user.client.EventListener;
import com.google.gwt.user.client.ui.Composite;
import com.google.gwt.user.client.ui.HasVisibility;
import com.google.gwt.user.client.ui.IsRenderable;
import com.google.gwt.user.client.ui.IsWidget;
import com.google.gwt.user.client.ui.SimplePanel;
import com.google.gwt.user.client.ui.UIObject;
import com.google.gwt.user.client.ui.Widget;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Supplier;
import org.atb.errai.demo.contactlist.client.local.App;
import org.atb.errai.demo.contactlist.client.local.NavBar;
import org.jboss.errai.ioc.client.api.EntryPoint;
import org.jboss.errai.ioc.client.container.ContextManager;
import org.jboss.errai.ioc.client.container.Factory;
import org.jboss.errai.ioc.client.container.FactoryHandleImpl;
import org.jboss.errai.ui.nav.client.local.Navigation;
import org.jboss.errai.ui.shared.DataFieldMeta;
import org.jboss.errai.ui.shared.Template;
import org.jboss.errai.ui.shared.TemplateUtil;
import org.jboss.errai.ui.shared.api.style.StyleBindingsRegistry;

public class Type_factory__o_a_e_d_c_c_l_App__quals__j_e_i_Any_j_e_i_Default extends Factory<App> { public interface o_a_e_d_c_c_l_AppTemplateResource extends Template, ClientBundle { @Source("org/atb/errai/demo/contactlist/client/local/App.html") public TextResource getContents(); }
  public Type_factory__o_a_e_d_c_c_l_App__quals__j_e_i_Any_j_e_i_Default() {
    super(new FactoryHandleImpl(App.class, "Type_factory__o_a_e_d_c_c_l_App__quals__j_e_i_Any_j_e_i_Default", EntryPoint.class, true, null, true));
    handle.setAssignableTypes(new Class[] { App.class, Composite.class, Widget.class, UIObject.class, Object.class, HasVisibility.class, EventListener.class, HasAttachHandlers.class, HasHandlers.class, IsWidget.class, IsRenderable.class });
  }

  public App createInstance(final ContextManager contextManager) {
    final App instance = new App();
    setIncompleteInstance(instance);
    final NavBar App_navbar = (NavBar) contextManager.getInstance("Type_factory__o_a_e_d_c_c_l_NavBar__quals__j_e_i_Any_j_e_i_Default");
    registerDependentScopedReference(instance, App_navbar);
    App_NavBar_navbar(instance, App_navbar);
    final SimplePanel App_content = (SimplePanel) contextManager.getInstance("ExtensionProvided_factory__c_g_g_u_c_u_SimplePanel__quals__j_e_i_Any_j_e_i_Default");
    registerDependentScopedReference(instance, App_content);
    App_SimplePanel_content(instance, App_content);
    final Navigation App_navigation = (Navigation) contextManager.getInstance("Type_factory__o_j_e_u_n_c_l_Navigation__quals__j_e_i_Any_j_e_i_Default");
    App_Navigation_navigation(instance, App_navigation);
    o_a_e_d_c_c_l_AppTemplateResource templateForApp = GWT.create(o_a_e_d_c_c_l_AppTemplateResource.class);
    Element parentElementForTemplateOfApp = TemplateUtil.getRootTemplateParentElement(templateForApp.getContents().getText(), "org/atb/errai/demo/contactlist/client/local/App.html", "body");
    TemplateUtil.translateTemplate("org/atb/errai/demo/contactlist/client/local/App.html", TemplateUtil.getRootTemplateElement(parentElementForTemplateOfApp));
    Map<String, Element> dataFieldElements = TemplateUtil.getDataFieldElements(TemplateUtil.getRootTemplateElement(parentElementForTemplateOfApp));
    final Map<String, DataFieldMeta> dataFieldMetas = new HashMap<String, DataFieldMeta>(2);
    dataFieldMetas.put("navbar", new DataFieldMeta());
    dataFieldMetas.put("content", new DataFieldMeta());
    Map<String, Widget> templateFieldsMap = new LinkedHashMap<String, Widget>();
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.App", "org/atb/errai/demo/contactlist/client/local/App.html", new Supplier<Widget>() {
      public Widget get() {
        return App_NavBar_navbar(instance).asWidget();
      }
    }, dataFieldElements, dataFieldMetas, "navbar");
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.App", "org/atb/errai/demo/contactlist/client/local/App.html", new Supplier<Widget>() {
      public Widget get() {
        return App_SimplePanel_content(instance).asWidget();
      }
    }, dataFieldElements, dataFieldMetas, "content");
    templateFieldsMap.put("navbar", App_NavBar_navbar(instance).asWidget());
    templateFieldsMap.put("content", App_SimplePanel_content(instance).asWidget());
    TemplateUtil.initWidget(instance, TemplateUtil.getRootTemplateElement(parentElementForTemplateOfApp), templateFieldsMap.values());
    StyleBindingsRegistry.get().updateStyles(instance);
    setIncompleteInstance(null);
    return instance;
  }

  public void generatedDestroyInstance(final Object instance, final ContextManager contextManager) {
    destroyInstanceHelper((App) instance, contextManager);
  }

  public void destroyInstanceHelper(final App instance, final ContextManager contextManager) {
    TemplateUtil.cleanupWidget(instance);
  }

  public void invokePostConstructs(final App instance) {
    instance.clientMain();
  }

  native static NavBar App_NavBar_navbar(App instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.App::navbar;
  }-*/;

  native static void App_NavBar_navbar(App instance, NavBar value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.App::navbar = value;
  }-*/;

  native static Navigation App_Navigation_navigation(App instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.App::navigation;
  }-*/;

  native static void App_Navigation_navigation(App instance, Navigation value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.App::navigation = value;
  }-*/;

  native static SimplePanel App_SimplePanel_content(App instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.App::content;
  }-*/;

  native static void App_SimplePanel_content(App instance, SimplePanel value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.App::content = value;
  }-*/;
}