package org.jboss.errai.ioc.client;

import com.google.gwt.core.client.GWT;
import com.google.gwt.dom.client.Element;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.HasClickHandlers;
import com.google.gwt.resources.client.ClientBundle;
import com.google.gwt.resources.client.ClientBundle.Source;
import com.google.gwt.resources.client.TextResource;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.Widget;
import java.lang.annotation.Annotation;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;
import javax.enterprise.context.Dependent;
import org.atb.errai.demo.contactlist.client.local.ContactDisplay;
import org.atb.errai.demo.contactlist.client.local.ContactListPage;
import org.atb.errai.demo.contactlist.client.local.NewContactPage;
import org.atb.errai.demo.contactlist.client.shared.Contact;
import org.atb.errai.demo.contactlist.client.shared.ContactService;
import org.jboss.errai.common.client.api.Caller;
import org.jboss.errai.common.client.ui.ElementWrapperWidget;
import org.jboss.errai.databinding.client.api.Convert;
import org.jboss.errai.databinding.client.api.DataBinder;
import org.jboss.errai.databinding.client.components.ListComponent;
import org.jboss.errai.ioc.client.container.ContextManager;
import org.jboss.errai.ioc.client.container.Factory;
import org.jboss.errai.ioc.client.container.FactoryHandleImpl;
import org.jboss.errai.ui.nav.client.local.TransitionTo;
import org.jboss.errai.ui.shared.DataFieldMeta;
import org.jboss.errai.ui.shared.Template;
import org.jboss.errai.ui.shared.TemplateUtil;
import org.jboss.errai.ui.shared.api.style.StyleBindingsRegistry;

public class Type_factory__o_a_e_d_c_c_l_ContactListPage__quals__j_e_i_Any_j_e_i_Default extends Factory<ContactListPage> { public interface o_a_e_d_c_c_l_ContactListPageTemplateResource extends Template, ClientBundle { @Source("org/atb/errai/demo/contactlist/client/local/ContactListPage.html") public TextResource getContents(); }
  public Type_factory__o_a_e_d_c_c_l_ContactListPage__quals__j_e_i_Any_j_e_i_Default() {
    super(new FactoryHandleImpl(ContactListPage.class, "Type_factory__o_a_e_d_c_c_l_ContactListPage__quals__j_e_i_Any_j_e_i_Default", Dependent.class, false, null, true));
    handle.setAssignableTypes(new Class[] { ContactListPage.class, Object.class });
  }

  public ContactListPage createInstance(final ContextManager contextManager) {
    final ContactListPage instance = new ContactListPage();
    setIncompleteInstance(instance);
    final Button ContactListPage_newcontact = (Button) contextManager.getInstance("ExtensionProvided_factory__c_g_g_u_c_u_Button__quals__j_e_i_Any_j_e_i_Default");
    registerDependentScopedReference(instance, ContactListPage_newcontact);
    ContactListPage_Button_newcontact(instance, ContactListPage_newcontact);
    final ListComponent ContactListPage_list = (ListComponent) contextManager.getContextualInstance("ContextualProvider_factory__o_j_e_d_c_c_ListComponent__quals__Universal", new Class[] { Contact.class, ContactDisplay.class }, new Annotation[] { });
    registerDependentScopedReference(instance, ContactListPage_list);
    ContactListPage_ListComponent_list(instance, ContactListPage_list);
    final TransitionTo ContactListPage_goToNewContact = (TransitionTo) contextManager.getContextualInstance("ContextualProvider_factory__o_j_e_u_n_c_l_TransitionTo__quals__Universal", new Class[] { NewContactPage.class }, new Annotation[] { });
    registerDependentScopedReference(instance, ContactListPage_goToNewContact);
    ContactListPage_TransitionTo_goToNewContact(instance, ContactListPage_goToNewContact);
    final Caller ContactListPage_contactService = (Caller) contextManager.getContextualInstance("ContextualProvider_factory__o_j_e_c_c_a_Caller__quals__Universal", new Class[] { ContactService.class }, new Annotation[] { });
    registerDependentScopedReference(instance, ContactListPage_contactService);
    ContactListPage_Caller_contactService(instance, ContactListPage_contactService);
    final DataBinder ContactListPage_binder = (DataBinder) contextManager.getContextualInstance("ContextualProvider_factory__o_j_e_d_c_a_DataBinder__quals__Universal", new Class[] { List.class }, new Annotation[] { });
    registerDependentScopedReference(instance, ContactListPage_binder);
    ContactListPage_DataBinder_binder(instance, ContactListPage_binder);
    o_a_e_d_c_c_l_ContactListPageTemplateResource templateForContactListPage = GWT.create(o_a_e_d_c_c_l_ContactListPageTemplateResource.class);
    Element parentElementForTemplateOfContactListPage = TemplateUtil.getRootTemplateParentElement(templateForContactListPage.getContents().getText(), "org/atb/errai/demo/contactlist/client/local/ContactListPage.html", "contact-list");
    TemplateUtil.translateTemplate("org/atb/errai/demo/contactlist/client/local/ContactListPage.html", TemplateUtil.getRootTemplateElement(parentElementForTemplateOfContactListPage));
    Map<String, Element> dataFieldElements = TemplateUtil.getDataFieldElements(TemplateUtil.getRootTemplateElement(parentElementForTemplateOfContactListPage));
    final Map<String, DataFieldMeta> dataFieldMetas = new HashMap<String, DataFieldMeta>(2);
    dataFieldMetas.put("list", new DataFieldMeta());
    dataFieldMetas.put("newcontact", new DataFieldMeta());
    Map<String, Widget> templateFieldsMap = new LinkedHashMap<String, Widget>();
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.ContactListPage", "org/atb/errai/demo/contactlist/client/local/ContactListPage.html", new Supplier<Widget>() {
      public Widget get() {
        return ElementWrapperWidget.getWidget(ContactListPage_ListComponent_list(instance).getElement());
      }
    }, dataFieldElements, dataFieldMetas, "list");
    TemplateUtil.compositeComponentReplace("org.atb.errai.demo.contactlist.client.local.ContactListPage", "org/atb/errai/demo/contactlist/client/local/ContactListPage.html", new Supplier<Widget>() {
      public Widget get() {
        return ContactListPage_Button_newcontact(instance).asWidget();
      }
    }, dataFieldElements, dataFieldMetas, "newcontact");
    templateFieldsMap.put("list", ElementWrapperWidget.getWidget(ContactListPage_ListComponent_list(instance).getElement()));
    templateFieldsMap.put("newcontact", ContactListPage_Button_newcontact(instance).asWidget());
    TemplateUtil.initTemplated(instance, TemplateUtil.getRootTemplateElement(parentElementForTemplateOfContactListPage), templateFieldsMap.values());
    ((HasClickHandlers) templateFieldsMap.get("newcontact")).addClickHandler(new ClickHandler() {
      public void onClick(ClickEvent event) {
        instance.newContact(event);
      }
    });
    DataBinder binder = ContactListPage_DataBinder_binder(instance);
    if (binder == null) {
      throw new RuntimeException("@AutoBound data binder for class org.atb.errai.demo.contactlist.client.local.ContactListPage has not been initialized. Either initialize or add @Inject!");
    }
    thisInstance.setReference(instance, "DataModelBinder", binder);
    binder.bind(ContactListPage_ListComponent_list(instance), "this", Convert.getConverter(List.class, List.class), null, false);
    StyleBindingsRegistry.get().updateStyles(instance);
    setIncompleteInstance(null);
    return instance;
  }

  public void generatedDestroyInstance(final Object instance, final ContextManager contextManager) {
    destroyInstanceHelper((ContactListPage) instance, contextManager);
  }

  public void destroyInstanceHelper(final ContactListPage instance, final ContextManager contextManager) {
    TemplateUtil.cleanupTemplated(instance);
    ((DataBinder) thisInstance.getReferenceAs(instance, "DataModelBinder", DataBinder.class)).unbind();
  }

  public void invokePostConstructs(final ContactListPage instance) {
    ContactListPage_setup(instance);
  }

  native static ListComponent ContactListPage_ListComponent_list(ContactListPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.ContactListPage::list;
  }-*/;

  native static void ContactListPage_ListComponent_list(ContactListPage instance, ListComponent<Contact, ContactDisplay> value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.ContactListPage::list = value;
  }-*/;

  native static DataBinder ContactListPage_DataBinder_binder(ContactListPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.ContactListPage::binder;
  }-*/;

  native static void ContactListPage_DataBinder_binder(ContactListPage instance, DataBinder<List<Contact>> value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.ContactListPage::binder = value;
  }-*/;

  native static TransitionTo ContactListPage_TransitionTo_goToNewContact(ContactListPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.ContactListPage::goToNewContact;
  }-*/;

  native static void ContactListPage_TransitionTo_goToNewContact(ContactListPage instance, TransitionTo<NewContactPage> value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.ContactListPage::goToNewContact = value;
  }-*/;

  native static Button ContactListPage_Button_newcontact(ContactListPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.ContactListPage::newcontact;
  }-*/;

  native static void ContactListPage_Button_newcontact(ContactListPage instance, Button value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.ContactListPage::newcontact = value;
  }-*/;

  native static Caller ContactListPage_Caller_contactService(ContactListPage instance) /*-{
    return instance.@org.atb.errai.demo.contactlist.client.local.ContactListPage::contactService;
  }-*/;

  native static void ContactListPage_Caller_contactService(ContactListPage instance, Caller<ContactService> value) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.ContactListPage::contactService = value;
  }-*/;

  public native static void ContactListPage_setup(ContactListPage instance) /*-{
    instance.@org.atb.errai.demo.contactlist.client.local.ContactListPage::setup()();
  }-*/;
}