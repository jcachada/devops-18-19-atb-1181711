package org.jboss.errai.marshalling.client.api;

import java.util.Set;
import org.atb.errai.demo.contactlist.client.shared.Contact;
import org.jboss.errai.marshalling.client.Marshalling;
import org.jboss.errai.marshalling.client.api.json.EJObject;
import org.jboss.errai.marshalling.client.api.json.EJValue;

public class Marshaller_o_a_e_d_c_c_s_Contact_Impl implements GeneratedMarshaller<Contact> {
  private Contact[] EMPTY_ARRAY = new Contact[0];
  private Marshaller<Long> java_lang_Long = Marshalling.getMarshaller(Long.class);
  private Marshaller<String> java_lang_String = Marshalling.getMarshaller(String.class);
  public Contact[] getEmptyArray() {
    return EMPTY_ARRAY;
  }

  public Contact demarshall(EJValue a0, MarshallingSession a1) {
    lazyInit();
    EJObject obj = a0.isObject();
    if (obj == null) {
      return null;
    }
    String objId = obj.get("^ObjectID").isString().stringValue();
    if (a1.hasObject(objId)) {
      return a1.getObject(Contact.class, objId);
    }
    Contact entity = new Contact();
    a1.recordObject(objId, entity);
    final Set<String> keys = obj.keySet();
    for (String key : keys) {
      if (key.equals("^EncodedType") || key.equals("^ObjectID")) {
        continue;
      }
      EJValue objVal = obj.getIfNotNull(key);
      if (objVal == null) {
        continue;
      }
      switch (key) {
        case "id": entity.setId(java_lang_Long.demarshall(objVal, a1));
        break;
        case "name": entity.setName(java_lang_String.demarshall(objVal, a1));
        break;
        case "email": entity.setEmail(java_lang_String.demarshall(objVal, a1));
        break;
        case "phoneNumber": entity.setPhoneNumber(java_lang_String.demarshall(objVal, a1));
        break;
      }
    }
    return entity;
  }

  public String marshall(Contact a0, MarshallingSession a1) {
    lazyInit();
    if (a0 == null) {
      return "null";
    }
    final boolean ref = a1.hasObject(a0);
    final String prefix = ((((((((("" + "{\"") + "^EncodedType") + "\":\"") + "org.atb.errai.demo.contactlist.client.shared.Contact") + "\"") + ",\"") + "^ObjectID") + "\":\"") + a1.getObject(a0)) + "\"";
    if (ref) {
      return prefix + "}";
    }
    return prefix + ((((((((((((((((("" + ",\"") + "id") + "\":") + java_lang_Long.marshall(a0.getId(), a1)) + ",\"") + "name") + "\":") + java_lang_String.marshall(a0.getName(), a1)) + ",\"") + "email") + "\":") + java_lang_String.marshall(a0.getEmail(), a1)) + ",\"") + "phoneNumber") + "\":") + java_lang_String.marshall(a0.getPhoneNumber(), a1)) + "}");
  }

  private void lazyInit() {

  }
}