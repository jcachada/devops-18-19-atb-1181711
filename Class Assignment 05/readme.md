# Class Assignment 05

## Installing Jenkins

Jenkins was installed in a Docker container using the following command:

```docker
docker run \
  -u root \
  --rm \  
  -d \
  -p 8080:8080 \
  -p 50000:50000 \
  -v jenkins-data:/var/jenkins_home \
  -v /var/run/docker.sock:/var/run/docker.sock \
  jenkinsci/blueocean
```

## Forking the repo

Since the steps for creating the first pipeline, as well as the code, are provided in the lecture slides, we'll ommit that part from the readme. Instead, we'll go over how to fork the repo into a private bitbucket repo, since that's the part that isn't made explicit in the lecture slides.

After opening the public repository in our browser, we clicked the "+" in the sidebar and selected "fork this repository." We set the repository to private and gave it a description to explain it was used for Class Assignment 05.

Then we created a local folder for the forked repository; we opened a new GIT bash window and typed in:

```git
git clone https://jcachada@bitbucket.org/jcachada/gradle_basic_demo-private-fork.git
```

This was enough to get our cloned fork repository onto our development machine.

We continued following the lecture slides.

## Class Assignment Starts Here

For the class assignment, we want to do the same we did in the lectures slides but for the gradle project we used in Class Assignment 02.

For this to work, and since Jenkins works with relative paths, we'll add the Jenkinsfile to the folder where our Class Assignment 02 - Part 1 (the gradle basic demo) was. However, we will also copy the Jenkinsfile, when we're done, to the Class Assignment 05 folder - this is done purely for convenience when indicating the relative paths, and is not a necessary step (the jenkinsfile could just be in the new CA05 folder.)

We created the following Jenkinsfile in that folder:

```jenkins
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: 'Bitbucket', url: 'https://jcachada@bitbucket.org/jcachada/devops-18-19-atb-1181711.git'
            }
        }
        stage('Assemble') {
            steps {
                echo 'Building...'
                sh './gradlew clean compileJava'
            }
        }
        stage('Test') {
            steps {
                echo 'Building...'
                sh './gradlew clean compileTestJava'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
        }
    }
}
```

Then, we want back to the jenkins dashboard and created a new job to execute that jenkinsfile. We pressed "New Item", selected "Pipeline" and called it "CA05". Then, under "pipeline", we selected "pipeline script from SCM". We added the link to our repository and the credentials for bitbucket we'd created during the lecture slides. We also gave Jenkins the relative path to our Jenkinsfile:

```Jenkins
ca2-part1/Jenkinsfile
```

We had to rename our directory for this (it was previously called Class Assignment 02 - Part 1) because the whitespace was messing up our relative path to the jenkinsfile in the Jenkins pipeline.

Now, when we press "build now", we get the following error:

```gradle
[Pipeline] sh
+ ./gradlew clean compileJava
/var/jenkins_home/workspace/CA05@tmp/durable-55c6f764/script.sh: line 1: ./gradlew: not found
```

This is because we're not running the ./gradlew command in the appropriate folder where the gradle wrapper is. So we need to navigate to that folder in the build stage of our jenkins pipeline. We edited the jenkinsfile to this:

```Jenkins
pipeline

    {
        agent any

        stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId:'Bitbucket', url:
                'https://jcachada@bitbucket.org/jcachada/devops-18-19-atb-1181711.git'
            }
        }
        stage('Assemble') {
            steps {
                dir('ca2-part1') {
                    echo 'Building...'
                    sh 'chmod +x gradlew'
                    sh './gradlew compileJava'
                }
            }
        }
        stage('Test') {
            steps {
                dir('ca2-part1') {
                echo 'Building...'
                sh 'chmod +x gradlew'
                sh './gradlew compileTestJava'
            }
            }
        }
        stage('Archiving') {
            steps {
                dir('ca2-part1') {
                sh 'pwd'
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
            }
        }
    }
    }
```

The dir command forces the steps to run in the correct subdirectory - the chmod command gives us executable permission on the gradlew wrapper.

Now we get:

```Jenkins
+ ./gradlew compileJava
Error: Could not find or load main class org.gradle.wrapper.GradleWrapperMain
```

This is because we have a global .gitignore file configured to ignore .jar files. Because of this, the .jar file of the gradle wrapper doesn't get added to our repository - which means that the gradle wrapper can't run properly. We need to manually force the .jar file of the gradle wrapper to be added to our repository in order for things to work. We opened a Git Bash and typed:

```git
git add -f ca2-part1/gradle/wrapper/gradle-wrapper.jar
```

Then we committed these changes and tried again.

Now everything worked - except for one thing! We weren't properly generating build files. Turns out we weren't using the correct command - compileJava doesn't actually build anything.

We wanted to use ./gradlew build, but there was a warning in the class lectures stating:

```text
Do not use the build task of gradle (because it also executes the tests)
```

So we decided to use the task in a way that doesn't run the tests:

We made the Assemble stage look like this:

```Jenkins
 stage('Assemble') {
            steps {
                dir('ca2-part1') {
                    echo 'Building...'
                    sh 'chmod +x gradlew'
                    sh './gradlew build -x test '
                }
            }
        }
```

The -x test annotation tells the build command in gradle to not run the tests. This time the build artifacts were properly generated and archived by Jenkins, as shown below!

![alt text](https://i.imgur.com/6HeZbgG.png "Build successful!")

![alt text](https://i.imgur.com/DDR4bxt.png "Archived things!")

Lastly, we want to publish junit test results to Jenkins. To do this, we edited the "test" stage to look like this:

```jenkins
 stage('Test') {
            steps {
                dir('ca2-part1') {
                echo 'Building...'
                sh 'chmod +x gradlew'
                sh './gradlew test'
                sh 'touch build/test-results/test/*.xml'
                junit 'build/test-results/test/*.xml'
            }
            }
        }
```

The chmod command gives the gradle wrapper permissions to be executed. Then we run the test task for gradle, which generates test reports. The shell command "touch" is used to force the timestamps of the test results to update, otherwise Jenkins would fail telling us that the test results are outdated. Lastly the "junit" step comes bundled with Jenkins and publishes the test results found in the indicated folder to Jenkins.

After we did this, the test results were done successfully, which meant that the Class Assignment was done (the first part, at least!)

Our final Jenkinsfile looks like this:

```jenkins

pipeline

    {
        agent any

        stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId:'Bitbucket', url:
                'https://jcachada@bitbucket.org/jcachada/devops-18-19-atb-1181711.git'
            }
        }
        stage('Assemble') {
            steps {
                dir('ca2-part1') {
                    echo 'Building...'
                    sh 'chmod +x gradlew'
                    sh './gradlew build -x test '
                }
            }
        }
        stage('Test') {
            steps {
                dir('ca2-part1') {
                echo 'Building...'
                sh 'chmod +x gradlew'
                sh './gradlew test' 
                sh 'touch build/test-results/test/*.xml'
                junit 'build/test-results/test/*.xml'
            }
            }
        }
        stage('Archiving') {
            steps {
                dir('ca2-part1') {
                sh 'pwd'
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/'
            }
            }
        }
    }
    }
```

And here are the published test results:

![alt text](https://i.imgur.com/OCNGytw.png "Test results").

## Tagging the CA

Finally, we used git add to add this readme file and commit it to the project. We tagged the final commit with the tag ca5-part1 using:

```git
git tag -a ca5-part1 -m "Finishes first part of class assignment."
git push origin ca5-part1
```