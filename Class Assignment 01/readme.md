# Class Assignment 01 Tutorial

We will consider for the purposes of this assignment that adding a copy of the demo to the bitbucket repository is not part of the assignment, since the assignment requirements mention that "You should use the errai-demonstration application that you already added to your
repository".

## Adding a tag

The first step of the assignment consisting of adding a tag to the already existing master branch. This was done with use of the git command:

```git
git tag -a v1.2.0 -m "Original Commit" 826af7c
```

Where the -a option tells git that the tag is an annotated tag and not a lightweight tag, meaning that it is checksummed and stored as a full object in the Git database, and the -m option gives git a message to go with the tag. The numbers after the message correspond to the number of the commit we want to add the tag to.

Normal pushes don't push tags to the repository - as such, the tag had to be individually pushed with the command:

```git

git push origin v1.2.0
```

## Creating a branch

After that, a branch was created to work on adding a new feature, phone-number. This branch was created using:

```git
git checkout phone-field
```

The git push command required an upstream to be set for the branch, through git push --set-upstream origin phone-field.

## Adding a feature

The following commits all happened on the branch phone-field.

A functionality was added, namely, support was added for a phone number to be introduced as well in the contact list. This was done by introducing several phoneNumber fields in relevant classes, namely in the Contact, NewContactPage and ContactDisplay java classes. Changes were also made to the .html files NewContactPage.html and ContactListPage.html. Most of these changes implied simply copying the implementation of other fields, like name and e-mail. For instance, where:

```java
@Inject
@DataField
@Bound
private TextBox email;
```

Was present, a similar Inject field was added with phoneNumber instead. The Contact class saw the most changes, with the introduction of a class attribute called phoneNumber, stored as a String, and the creation of getter/setter methods for that attribute. The changes were committed to the repository with the use of git commit -a, followed by git push.

## Testing the new feature

To test the new feature, the first change was updating the Maven dependency so it used the most current version of JUnit. This was done by updating the pom.xml file and re-importing the maven dependencies.

After this change, a new class was created and added to the test folder, called ContactTest. There, the methods in the Contact.java class received unitary tests according to JUnit's methodology, sorted by the Arrange / Act / Assert method and making use of AssertEquals.

Once again, the changes were pushed to the repository by using git commit -a followed by git push. But, this time, new files were added, namely ContactTest.java, so before the git commit -a, we needed to make sure all the files were tracked. To do that, we navigated to the Demo folder using the cd command, and then used git add . to track all the files in the folder. After this command, git commit -a and git push were enough to push the new test class into version control.

## Gitignore

At this point we realized several runtime and superfluous files were being tracked by version control. As such, we created a .gitignore file with the keywords Java, IntelliJ, Maven and GWT at <https://www.gitignore.io/.> Using nano .gitignore, and pasting the generated code into nano, we created the file from the terminal. After that, git add .gitignore, followed by git commit -a and git push, made sure the gitignore file reached the repository.

## Merge

Now that the feature was implemented and tested, we decided to merge it with the master branch. We did this from the phone-field branch, by using:

```git
git checkout master
git merge phone-field
git push
```

This made sure master was up to date with phone-field and changed us back to working on the master branch, now that the phone-field branch was no longer needed. We kept the issue open and the branch not deleted to use it later for validations.

At this point the bitbucket commits that previously showed a "phone-field" indicator had that indicator removed.

## Adding a new tag

Once the functionality was implemented, a new tag was added, this time v1.3.0. We used:

```git
git tag -a v1.3.0 -m "Adds phoneNumber functionality and tests" d266738
git push origin v1.3.0
```

This tag marks the introduction of the phone number functionality.

## Bug fixes and validations

To add validations and fix bugs, a new branch was created, called validations, with git branch validations, followed by git checkout validations.

Validations were added to the main constructor methods of the Contact.java class, and tests were added to the ContactTest.java class.

These changes were then commited through git commit -a , followed by git push. The git push command required an upstream to be set for the branch, through git push --set-upstream origin validations.

![alt text](https://i.imgur.com/AFRIyQD.png "Branch in Bitbucket")

Once again the bitbucket commit shows the branch indicator.

We now switched to branch phone-field to add a validation for the phone number: no letters allowed. We added the necessary changes to the code and the new tests, and committed the changes to branch phone-field using git commit -a and git push.

## Final merge into master

With all the validations added, both our branches need to be merged into master.

To do this:

```git
git checkout master
git merge validations
git push
git merge phone-field
git push
```

## Conflict resolution

In the final merge some conflicts showed up that couldn't be resolved automatically by git. In this case, they had to be resolved manually. The resolved conflicts were committed again in the usual manner.

## Adding tag

The tag for the new version, with added validations, is v1.3.1 . It was created with:

```git
git tag -a v1.3.1 -m "Adds validations for phone number and e-mail attributes" 368c3aa
git push v1.3.1
```

## Deleting old branches

It is good practice do delete old branches that are no longer needed. That is the case with the branches phone-field and validations that we used to implement new functionalities.

Those branches were deleted at this point with:

```git
git branch -d phone-field
git branch -d validations
```

## Readme file

The readme file was created in Visual Studio Code with markdown assistance enabled, and then saved onto the repository folder. git add readme.md was used to track the file, then git commit -m "Adds readme" and git push were used.