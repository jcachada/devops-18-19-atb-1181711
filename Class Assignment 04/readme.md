# Class Assignment 04

The first thing we did was installing Docker from <https://www.docker.com/>. Since we're using Windows 10 Pro, we installed Docker and not Docker Toolbox.

Once Docker was up and running, we downloaded the demo from <https://github.com/atb/docker-compose-demo>. We placed it in the folder reserved for Class Assignment 04 in our repository (where this readme was also placed).

At this point we opened a terminal and tried docker-compose up.

Just from this, it was possible to access the H2 console at <http://localhost:8082> . We were also able to access Wildfly at <http://localhost:8080>. However, we were not able to access the errai-demonstration application at http://localhost:8080/errai-demonstration-gradle. Instead, we were met with 404 - Not Found, as shown in the message below.

![alt text](https://i.imgur.com/bdU6LXY.png "Failed demo")

We're missing the actual .war of errai-demonstration gradle, plus a few commands that we had to change in the previous class assignment to make sure everything works properly: for instance, we have to change the connection string to the database so that the database that is generated is not in memory, and change the access password to the database. We could do this manually (to see how one does this manually, check the Class Assignment 03 readme), but we want Docker to do it for us, so we will add the following commands to the web Dockerfile:

```docker
sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:tcp://db:9092//usr/src/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
  sed -i 's#<password>sa</password>#<password></password>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml
```

This way, both of the problems above are solved by Docker. We need to change the connection url from "localhost" to the name of the database container, in this case "db", so that the application gets properly deployed.

Besides all this, we still need the actual .war file to deploy the application. To do this, first we generate the .war by executing the build command on the application. Then, we copied the .war file to the web folder we had created, because we need to make the .war file easily accessible for Docker to copy it into the container.

Then we opened the web Dockerfile and added the following command:

```docker
COPY /errai-demonstration-gradle.war /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/
```

This command makes sure that the .war of the application gets copied into the container's Wildfly deployments folder, which means that the deployment can now happen properly. Let's test it by stopping the containers, and then doing:

```docker
docker-compose down
docker-compose build
docker-compose up
```

This rebuilt the containers and provided them with the changes in the new dockerfiles. And there we go - this time the application deployed properly and is accessible through <http://localhost:8080/errai-demonstration-gradle/>.

![alt text](https://i.imgur.com/6WIHHI3.png "Successful deployment")

All we need to do now is add persistence, so that our files aren't lost when the containers are destroyed.

## Adding persistence

There is already a shared volume declared in the docker-compose file: this folder is called "data" on our local system. Therefore, all we need to do is change where H2 stores its own files and make it store the files in the shared folder.

To do this, we once again went to the web container's Dockerfile, and we changed the connection URL from what we had previously changed it to in the sed command, to:

```docker
  sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
```

This way, the database files created by H2 are saved in the usr/src/data folder, in a file called data.db. This folder is a volume declared in the Docker-Compose file, so it will be persisted into our host machine, as shown in the screenshot below:

![alt text](https://i.imgur.com/m1sTN30.png "Persisted database file")

With these changes, the database is successfully persisted, and all the entries we saved are still there after we destroy the containers and recreate them again.

![alt text](https://i.imgur.com/nCHs1MG.png "Entries are persisted!")

## Committing to docker repository

FIrst, we need to create new images from our changed containers, and we need to give them a name so we can push them to the Docker repository.

We start with:

```bash
docker commit 81d51edca2c2 ca04-topush-web
```

Where 81d51edca2c2 is the ID of our web container we got from doing docker-compose ps.

Now, if we run:

```bash
docker images
```

We can see the image we just created, as shown in the screenshot below:

![alt text](https://i.imgur.com/hFwrQXE.png "Created image").

Now we need to push it to the dockerhub. First, we went to the dockerhub website and logged in with our docker credentials.

Then, we created a new public repository called devopstestca04.

Back to our command line, we typed in:

```docker
docker login
```

Since we're using Docker Desktop on Windows 10 Pro, and we're already logged in to Docker, it validated our credentials automatically.

Next, we tagged the local image we just created. This tells Docker that the image has the tag to a specific repository. The :web portion is the name of the tag in dockerhub.

```docker
docker tag ca04-topush-web jcachada/devopstestca04:web
```

Finally, we pushed it to our repository with:

```docker
docker push jcachada/devopstestca04
```

The push was successful, as seen in the following screenshot.

![alt text](https://i.imgur.com/YrsaHVz.png "Pushed web").

So now we did the same for the db container, with:

```docker
docker commit 383e427510ee ca04-topush-db
docker tag ca04-topush-db jcachada/devopstestca04:db
docker push jcachada/devopstestca04
```

Everything worked fine, as we can see:

![alt text](https://i.imgur.com/hgl35z2.png "Pushed both").

This concludes the class assignment -- all that's left is to push the readme to our personal repository and tag it as Class Assignment 04.

Our Dockerhub repository can be accessed here: <https://cloud.docker.com/repository/docker/jcachada/devopstestca04/>.

## Push and tag

To commit and tag our class assignment to our repository, we used, from a GIT Bash in our repository folder:

```git
git add -A
git commit -a -m "Adds readme for class assignment 04."
git push origin master```

And we tagged the commit as we've been doing since Class Assignment 01 (Check CA01's readme for more information on how to tag commits.)