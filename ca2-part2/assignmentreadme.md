# Class Assignment 02 Part 02 Tutorial

## Creating a branch

After that, a branch was created to work on adding a new feature, phone-number. This branch was created using:

```git
git checkout -b gradle-plantuml
```

The git push command required an upstream to be set for the branch, through git push --set-upstream origin phone-field.

## Cloning the initial project

The first step of the assignment consisting of cloning the existing demo available at <https://github.com/atb/plantuml-gradle.> This was done by manually downloading the files associated with the build, then placing them on the folder reserved for Class Assignment 02 through File Explorer.

Following that, the command:

```git
git
git add .
```

Was used. It was followed by:

```git
git
git commit -a -m "Adds calculator project for part 2 of class assignment 2."
git push origin master
```

This was enough to add the cloned project to our existing repository.

## Experimenting with gradle

The next step was experimenting with the project and the available commands listed by the professor.

After navigating to the project's folder with a terminal and the cd command, we used:

```gradle
./gradlew renderPlantUml
```

At this point installing graphViz was needed - to do so we simply downloaded the executable from <https://graphviz.gitlab.io/_pages/Download/Download_windows.html> and re-ran the command above. This generated the images we were looking for inside build/puml/com/twu/calculator, as shown in the images below.

![alt text](https://i.imgur.com/Fd6Ly9o.png "Rendered images")

![alt text](https://i.imgur.com/63v9ta1.png "Rendered images")

After going through and looking at the files inside the buildSrc folder, specifically the render() method in the class RenderPlantUmlTask that is annotated with @TaskAction, we moved on to the javadocs.

In the terminal, we typed:

```gradle
./gradlew javadoc
```

This created the docs folder with the subpath docs/javadoc/com/twu/calculator and all the other content necessary for javadocs, as shown below.

![alt text](https://i.imgur.com/XaKKVau.png "Created javadocs")

However, the index.html file here references two images that were created in the wrong folder, so opening index.html in the browser shows two broken images. 

![alt text](https://i.imgur.com/fFgvCpN.png "Broken images")

Manually copying the generated images from the wrong folder into docs/javadoc/com/twu/calculator fixed this.

![alt text](https://i.imgur.com/ZnuCBPo.png "Fixed images")

## Automating the task

At this point we wanted to automate the task, so we created an issue on bitbucket (#7) to do so.

We then opened build.gradle in our IDE - the project's build.gradle, and not the one in buildSrc.

We added:

```groovy
task copyUML(type: Copy) {
    from 'build/puml/com/twu/calculator'
    into 'build/docs/javadoc/com/twu/calculator'
}
```

This task copies all contents from the generated plantUMLs to the correct folder, so they're in the right place and show up in the index.html.

However, we still wanted to automate further! We wanted to make sure we could have just one task that creates the javadocs with all the images in the right place, without having to run the tasks individually. As thins are right now, we'd have to do:

```gradle
./gradlew run javadoc
./gradlew run renderPlantUml
./gradlew run copyUML
```

That's too many lines! To avoid this, we introduced dependencies to the task we added:

```groovy
task copyUML(type: Copy) {
    dependsOn 'javadoc'
    from 'build/puml/com/twu/calculator'
    into 'build/docs/javadoc/com/twu/calculator'
}
```

This means that using ./gradlew run copyUML will now run the javadoc task first. But we still need to render the plantUMLs, so we added the following dependency to javadoc:

```groovy
javadoc {
    dependsOn 'renderPlantUml'
    source = sourceSets.main.allJava
    options.overview = "src/main/javadoc/overview.html" // relative to source root
}
```

This means that javadoc will first run the renderPlantUml task. Now all is fully automated: we can just run the copyUML task and everything will go to the correct place. To test that everything is working properly, we used:

```gradle
./gradlew clean
./gradlew copyUML
```

We got:

```gradle
PS Z:\MEGA\ISEP\DevOps\Class Assignment 02 Part 2\Calculator> ./gradlew copyUML

BUILD SUCCESSFUL in 2s
4 actionable tasks: 4 executed
PS Z:\MEGA\ISEP\DevOps\Class Assignment 02 Part 2\Calculator>
```

And all the files were in their correct places:

![alt text](https://i.imgur.com/ZXfkP81.png "Successful automation")

We commited the changes with:

```git
git commit -a -m "Ref #7:Introduces automation for the problem mentioned in readme."
git push origin gradle-plantuml
```

## Replicate the solution on errai-demonstration

## Clone the project

Now it's time to replicate the solution for the errai-demonstration we had been working with.

The first step of the assignment consisting of cloning the existing demo available at <https://github.com/atb/errai-demonstration-gradle>. This was done by manually downloading the files associated with the build, then placing them on the folder reserved for Class Assignment 02 / Errai-Demonstration through File Explorer.

Following that, the command:

```git
git
git add .
```

Was used. It was followed by:

```git
git
git commit -a -m "Refs #8 - Adds gradle version of errai-demonstration."
git push origin gradle-plantuml
```

This was enough to add the cloned project to our existing repository.

![alt text](https://i.imgur.com/tMUJ9QX.png "Push with branch")

## Study the readme

The first thing we did is open a command line, then navigate to the errai-demonstration folder and run ./gradlew clean.

This resolved all the dependencies and gave back:

```gradle
 ./gradlew clean

> Configure project :
POM relocation to an other version number is not fully supported in Gradle : xml-apis:xml-apis:2.0.2 relocated to xml-apis:xml-apis:1.0.b2.
Please update your dependency to directly use the correct version 'xml-apis:xml-apis:1.0.b2'.
Resolution will only pick dependencies of the relocated element.  Artifacts and other metadata will be ignored.

BUILD SUCCESSFUL in 19s
1 actionable task: 1 up-to-date
```

We then used:

```gradle
./gradlew provision
```

To download Wildfly. This download took a while for the first time.

![alt text](https://i.imgur.com/JzRLNLF.png "Wildfly download")

Finally Wildfly finished downloading (after 10 whole minutes!), so we carried on and ran ./gradlew build.

However, we kept getting an error:

```gradle
* What went wrong:
Execution failed for task ':gwtDependencies'.
BUILD FAILED in 0s
4 actionable tasks: 1 executed, 3 up-to-date
Z:\MEGA\ISEP\DevOps\Class Assignment 02 Part 2\Errai-Demonstration\build\gwt\deps\errai-javaee-all-4.1.0-SNAPSHOT.jar: A required privilege is not held by the client.
```

We decided to inspect the task, and noticed this:

```gradle
[WARNING]
//  To create symbolic links in Windows, the shell needs to be run with administator privileges,
//  _even_ if you're a user with administrator privileges.
```

So we tried again, this time running the command line as an administrator. This didn't work either, so we googled around a bit, and found that it seemed to be a problem with User Account Control. So we disabled UAC on Windows 10: we navigated to Local Security Policy, then Local Policies, Security Options, and changed User Account Control: Run all Administrators in Admin Approval Mode to "Disabled". 

This fixed the problem, but now we had a new one:

```gradle
Error: Could not find or load main class com.google.gwt.dev.Compiler

FAILURE: Build failed with an exception.

* What went wrong:
Execution failed for task ':gwtCompile'.
> Process 'command 'C:\Program Files\Java\jdk1.8.0_201\bin\java.exe'' finished with non-zero exit value 1
```

After some more googling around, we found that java's Windows classpath separator is ;, as opposed to Unix systems' :. The error was fixed by changing:

```groovy
def gwtClasspath() {
  [gwt.dir.input, "${gwt.dir.deps}/*"].flatten().join(":")
}
```

To:

```groovy
def gwtClasspath() {
  [gwt.dir.input, "${gwt.dir.deps}/*"].flatten().join(";")
}
```

This solved the issue, but another error popped up. At this point we checked Moodle, and realized a new build had been provided; so we replaced the old build.gradle file with the new one and tried again. This time the build was successful in 1m10s!

At this point we ran ./gradlew startWildfly to start wildfly. At first nothing showed up in  <http://localhost:8080/errai-demonstration-gradle>, but then we realized that the link to use needed to match the name of our created folder / directory in the filesystem, so we changed the link to <http://localhost:8080/Errai-Demonstration>, and the application popped up.

We experimented with starting and stopping wildfly a couple of times, and with using "./gradlew redeploy" to refresh changes, before moving on to the next task.

## Update gradle to add the javadoc and renderPlantUml tasks

The first thing we did is update the build.gradle script to include:

```groovy
task renderPlantUml(type: RenderPlantUmlTask) {
}

javadoc {
    dependsOn 'renderPlantUml'
    source = sourceSets.main.allJava
    options.addStringOption("sourcepath", "")
    options.overview = "src/main/javadoc/overview.html"
}
```

Since the renderPlantUml task depends on things that are compiled before the actual program, we had to create a buildSrc folder. To do this we copied the buildSrc folder from the calculator project in part 1 of the class assignment 02, pasted it into the project root. We had to manually mark \buildSrc\src\main\groovy as a sources folder using the IDE's project structure feature (Ctrl+ Alt + Shift + S in intelliJ). This buildSrc folder contains the actual code for renderPlantUml to run. 

Lastly, because the following line exists:

```groovy
  def Path assetsPathInput = project.projectDir.toPath().resolve('src/main/puml/'))
```

 We had to create the folder src/main/puml, otherwise the task would fail when executed. The renderPlantUml task takes uml sequence diagrams from \src\main\puml and renders images from them into build\puml. This means that we need actual sequence diagrams to begin with. For demonstration purposes - and since this assignment is about gradle automation, and not about writing plantUML diagrams - we used the previously provided .puml files from part 1. We then ran:

```gradle
 ./gradlew renderPlantUml
```

 The files were successfully rendered and placed inside build/puml.

 We then added the following task:

```groovy
 task copyUML(type: Copy) {
    dependsOn 'javadoc'
    from 'build/puml/'
    into 'build/docs/javadoc/org/atb/errai/demo/contactlist/client/local'
}
```

Once again, we're just copying the files for demonstration purposes - the diagrams won't match the functionality / classes. Since javadoc depends on renderPlantUml and copyUML depends on javadoc, we can now just do:

```gradle
./gradlew clean
./gradlew build
./gradlew copyUML
```

And all the proper tasks will be executed successfully in our new project.

We then committed and pushed all changes to our branch using the command line.

## Merge the branch

Seeing as our feature is now added, we merged the branch by doing:

```git
git checkout master
git merge gradle-plantuml
git push origin master
```

![alt text](https://i.imgur.com/CfhLxYm.png "Merge")

We then deleted the branch we had created (as it is good practice to delete branches after finishing working on them).

This was done with:

```git
git branch -d gradle-plantuml
```

## Tagging the final commit

At this point we added a tag to the last commit. This was done with:

```git
git tag ca2-part2 4f5b081
git push origin ca2-part2
```.