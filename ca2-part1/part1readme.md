# Class Assignment 02 Part 01 Tutorial

## Cloning the initial project

The first step of the assignment consisting of cloning the existing demo available at <https://bitbucket.org/luisnogueira/gradle_basic_demo/src.> This was done by manually downloading the files associated with the build, then placing them on the folder reserved for Class Assignment 02 through File Explorer.

Following that, the command:

```git
git add .
```

Was used. The following error message was returned:

![alt text](https://i.imgur.com/n37MMl4.png "Error adding directory.")

To solve this, we navigated to the folder where the cloned project was placed, and manually deleted the hidden .git folder that was present there. Afterwards, git add . worked as intended.

It was followed by:

```git
git commit -a -m "Refs #2 . Clones base project for second class assignment, part 1."
git push origin master
```

This was enough to add the cloned project to our existing repository.

## Gradle wrapper

To use the gradle wrapper in the cloned project after reading the .readme file made available by the professor, we simply executed the gradlew.bat file.

## Experimenting with gradle

The next step was experimenting with the project and the available commands listed by the professor.

```gradle
./gradlew build
> Task :compileJava UP-TO-DATE
> Task :processResources UP-TO-DATE
> Task :classes UP-TO-DATE
> Task :jar UP-TO-DATE
> Task :startScripts UP-TO-DATE
> Task :distTar UP-TO-DATE
> Task :distZip UP-TO-DATE
> Task :assemble UP-TO-DATE
> Task :compileTestJava NO-SOURCE
> Task :processTestResources NO-SOURCE
> Task :testClasses UP-TO-DATE
> Task :test NO-SOURCE
> Task :check UP-TO-DATE
> Task :build UP-TO-DATE
BUILD SUCCESSFUL in 0s
```

After this, the second command was run:

```gradle
java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001
```

The Windows Firewall popped up, and we had to allow Java to bypass standard firewall protection. After this, we received the feedback message that "the chat server is running". We opened a new terminal and typed in:

```gradle
./gradlew runClient
```

This brought up a java "chatter" window, asking us to choose a screen name. We tried choosing a screen name, then opened two more terminals and ran the command above again so we would have multiple clients running. These clients allowed for no chatting / input besides the screen name.

## Add a new task to run the server

To add a task, the build.gradle file was opened in an IDE, namely IntelliJ Idea Ultimate Edition. At the bottom of the file, the following code was added:

```groovy
task runServer(type:JavaExec, dependsOn: classes){
    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatServerApp'

    args '59001'
}
```

To check that the task had been added successfully, we opened a terminal window on the project's root and typed: 

```gradle
./gradlew tasks --all
```

This showed at the bottom, under "Other tasks": 

```gradle
Other tasks
-----------
compileJava - Compiles main Java source.
compileTestJava - Compiles test Java source.
processResources - Processes main resources.
processTestResources - Processes test resources.
runClient
runServer
startScripts - Creates OS specific scripts to run the project as a JVM application.
```

Which means that the task was added successfully, since it is listed there.

At this point, we committed the changes with git commit -a and git push origin master.

## Adding a unit test

The first thing we did to make sure the unit test could be added is add the proper dependencies. Once again, we opened our build.gradle file in our IDE. Under "dependencies", we added the dependency for junit 4.12, which we got from: <https://mvnrepository.com/artifact/junit/junit/4.12>.

The line we added looked like:

```groovy
testCompile group: 'junit', name: 'junit', version: '4.12'
```

We then copied the code made available by the professor in the slides to a class created with the help of the IDE. We had to create the test directory under src, then java under test, then basic_demo under java, and finally AppTest.java inside basic_demo. The final path looked like:

```java
src/test/java/basic_demo/AppTest.java
```

We pasted the unitary test's code into that class, we tried running the unitary test and running % ./gradlew build again to make sure everything was working, and once again committed the changes with git commit -a and git push origin master. This time we had to make sure the new class was being tracked first, so we used:

```git

git add src/test/
git commit -a
git push origin master
```

## Adding a "copy" task

Next we wanted to add a copy task to gradle's task list. We opened build.gradle in the IDE again, and added:

```groovy
task copyDocs(type: Copy) {
    from 'src'
    into 'build/backup'
}
```

To make sure the task had been added properly, we used:

```gradle
./gradlew tasks --all
```

And, when the new CopyDocs task showed up in the task list, under "Other tasks", we tried running it to make sure everything was working properly. We did this with:

```gradle
./gradlew copyDocs
```

And got:

```gradle
> Task :copyDocs

BUILD SUCCESSFUL in 0s
1 actionable task: 1 executed
```

Just to make sure, we checked if the backup folder had been created properly manually in the file explorer, and there it was:

![alt text](https://i.imgur.com/4WPbkx0.png "Backup was created properly.")

We then committed and pushed all changes through the command line again.

## Adding a "zip" task

Next we wanted to add a zip task to gradle's task list. We opened build.gradle in the IDE again, and added:

```groovy
task myZip(type: Zip) {
    from 'src'
    destinationDir(file('build/zip'))
}
```

To make sure the task had been added properly, we used:

```gradle
./gradlew tasks --all
```

And, when the new myZip task showed up in the task list, under "Other tasks", we tried running it to make sure everything was working properly. We did this with:

```gradle
./gradlew myZip
```

And got:

```gradle
> Task :myZip

BUILD SUCCESSFUL in 1s
1 actionable task: 1 executed
```

Just to make sure, we checked if the zip file had been created properly, and in the correct folder, manually in the file explorer, and there it was:

![alt text](https://i.imgur.com/ZmD8KKu.png "Zip was created properly.")

We then committed and pushed all changes through the command line again.

## Adding the readme

Finally, we used git add to add this readme file and commit it to the project. Since there was already a readme.md file, we named it "assignmentreadme.md". We tagged the final commit with the tag ca2-part1 using:

```git
git tag -a ca2-part1 -m "Finishes first part of class assignment."
git push origin ca2-part1
```