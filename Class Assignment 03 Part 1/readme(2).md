# Class Assignment 03 Part 01 Tutorial

## Starting the VM

After the installation was completed, we installed the tools needed for the task. 

We configured the networking by using:

```bash
sudo apt install net-tools
```

And then:

```bash
sudo nano /etc/netplan/01-netcfg.yaml
```

This was used to edit the configuration file for the networking. In this case, we set the new accessable IP to 192.168.56.100/24.

First, we used:

```bash
sudo apt install openssh-server
```

Then:

```bash
sudo apt install vsftpd
```

Both the installs were successful, so now we had to enable write access for vstfpd. 

```bash
sudo nano /etc/vsftpd.conf
```

We uncommented the line that read "write-enable=YES". We used:

```bash
sudo service vsftpd restart
```

To restart the service.

## Connecting to the VM From host

At this point we went back to the host machine and started a command line terminal. Then, we used:

```terminal
ssh joao@192.168.56.100
```

The connection was successful!

![alt text](https://i.imgur.com/acPGMtN.png "Proper connection")

At this point we were able to install git and java 8's sdk from the host machine's terminal - we didn't have to use the virtual machine.

## Errai-demonstration

To clone the errai project, we used:

```git
git clone https://github.com/atb/errai-demonstration-gradle.git
```

The project was cloned successfully into our VM. We then executed the gradle commands to start the application - but first we moved onto the new directory with "cd ./errai-demonstration-gradle".

```bash
./gradlew provision
./gradlew build
./gradlew startWildfly
```

Provision took a whole 17 minutes, but it finally managed to download the necessary dependencies. We then moved on to build. To edit the wildfly file to accept any address, we navigated to the folder where standalone.xml was in using the cd command, then we used nano standalone.xml to open the file and edit the line to accept any address. Then, we restarted wildfly with ./gradlew stopWildfly and then ./gradlew startWildfly.

We then attempted to access the application on the IP address we had configured - 192.68.56.100:8080. Success! The application loaded properly in the host machine.

![alt text](https://i.imgur.com/hW28CIk.png "Application loaded")

## Part 1 Starts Here

After the VM is properly set up, we cloned our individual repository on the virtual machine, using:

```git
git clone https://jcachada@bitbucket.org/jcachada/devops-18-19-atb-1181711.git
```

This cloned our individual repository to the VM, which means that all the projects we'd been working on so far are now available within the VM. We tried running each of them.

## Running the maven project

To run the first errai-demonstration demo, we had to first get maven with "sudo apt install maven". Then we navigated to the project's directory on our individual repository and ran mvn gwt:run.

This failed after resolving some of the dependencies. It failed because we don't have a desktop in our Virtual Machine, and we're missing some important things to run maven's gwt:run - for instance, we're missing the proper java jdk.

![alt text](https://i.imgur.com/DedTGXV.png "Failed demo")

A possible workaround this would be to deploy wildfly manually - we'd have to configure it to accept remote connections (any address) and then run wildfly with manual commands.

## Running the gradle projects

## CA 02 Part 1

We then tried to run the gradle projects. We used the cd command in the command line to navigate to the folders where gradle projects were - for instance, Class Assignment 02 Part 1.

First things first - we need to install gradle. To do that, we ran sudo apt install gradle.

There, we used gradle commands to run the projects.

```gradle
gradle build
```

The project finished building successfully.

We then tried running the tasks we had created in the class assignment.

We tried:

```gradle
gradle copyDocs
gradle myZip
```

Both the tasks worked properly and the files were created in the right places!

## CA 02 Part 2

We navigated to the folder in part 2 using the cd command. Here we just tried running the "parent" task which all the other tasks depended on, since this task already means all the other tasks we wrote get executed. We did:

```gradle
gradlew clean
gradlew build
gradlew copyUML
```

All of the commands failed with the same error:

```gradle
* Where:
Build file '/home/joao/devops-18-19-atb-1181711/Class Assignment 02 Part 2/Errai-Demonstration/build.gradle' line: 156

* What went wrong:
A problem occurred evaluating root project 'Errai-Demonstration'.
> Could not get unknown property 'classesDirs' for main classes of type org.gradle.api.internal.tasks.DefaultSourceSetOutput.

* Try:
Run with --stacktrace option to get the stack trace. Run with --info or --debug option to get more log output.

BUILD FAILED
```

This is because the Ubuntu installation we're running is too barebones for the projects to work properly - we're using a version with no desktop, for instance, and missing a lot of other utilities that usually come packaged by default with general use distributions for common operating systems.

## Readme and tag

The readme was written and the tag was created through the command line (git tag -a), then pushed into our personal repository with git push, as already extensively described in previous class assignments.